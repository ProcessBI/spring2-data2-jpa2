package com.btool.repository;

import com.btool.domain.CriticalDataColumnMapping;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CriticalDataColumnMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CriticalDataColumnMappingRepository extends JpaRepository<CriticalDataColumnMapping, Long> {

}
