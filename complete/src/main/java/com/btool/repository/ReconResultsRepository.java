package com.btool.repository;

import com.btool.domain.ReconResults;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconResults entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconResultsRepository extends JpaRepository<ReconResults, Long> {

}
