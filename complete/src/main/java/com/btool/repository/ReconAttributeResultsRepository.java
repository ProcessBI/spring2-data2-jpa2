package com.btool.repository;

import com.btool.domain.ReconAttributeResults;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconAttributeResults entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconAttributeResultsRepository extends JpaRepository<ReconAttributeResults, Long> {

}
