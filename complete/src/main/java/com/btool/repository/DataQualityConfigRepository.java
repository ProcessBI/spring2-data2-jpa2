package com.btool.repository;

import com.btool.domain.DataQualityConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DataQualityConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataQualityConfigRepository extends JpaRepository<DataQualityConfig, Long> {

}
