package com.btool.repository;

import com.btool.domain.DataQualityRule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DataQualityRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataQualityRuleRepository extends JpaRepository<DataQualityRule, Long> {

}
