package com.btool.repository;

import com.btool.domain.ReconAttributeLevelBreaks;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconAttributeLevelBreaks entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconAttributeLevelBreaksRepository extends JpaRepository<ReconAttributeLevelBreaks, Long> {

}
