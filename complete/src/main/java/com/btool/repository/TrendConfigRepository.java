package com.btool.repository;

import com.btool.domain.TrendConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TrendConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrendConfigRepository extends JpaRepository<TrendConfig, Long> {

}
