package com.btool.repository;

import com.btool.domain.DatasourceProperties;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DatasourceProperties entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatasourcePropertiesRepository extends JpaRepository<DatasourceProperties, Long> {

}
