package com.btool.repository;

import com.btool.domain.RunHistory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RunHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RunHistoryRepository extends JpaRepository<RunHistory, Long> {

}
