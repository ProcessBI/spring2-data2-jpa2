package com.btool.repository;

import com.btool.domain.DatasetColumn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DatasetColumn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatasetColumnRepository extends JpaRepository<DatasetColumn, Long> {

}
