package com.btool.repository;

import com.btool.domain.HolidayCalendar;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HolidayCalendar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HolidayCalendarRepository extends JpaRepository<HolidayCalendar, Long> {

}
