package com.btool.repository;

import com.btool.domain.DataQualityResults;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DataQualityResults entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataQualityResultsRepository extends JpaRepository<DataQualityResults, Long> {

}
