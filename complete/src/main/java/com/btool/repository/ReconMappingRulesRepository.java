package com.btool.repository;

import com.btool.domain.ReconMappingRules;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconMappingRules entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconMappingRulesRepository extends JpaRepository<ReconMappingRules, Long> {

}
