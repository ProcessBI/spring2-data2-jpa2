package com.btool.repository;

import com.btool.domain.RefdataContext;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RefdataContext entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefdataContextRepository extends JpaRepository<RefdataContext, Long> {

}
