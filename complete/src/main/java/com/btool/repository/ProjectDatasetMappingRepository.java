package com.btool.repository;

import com.btool.domain.ProjectDatasetMapping;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProjectDatasetMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProjectDatasetMappingRepository extends JpaRepository<ProjectDatasetMapping, Long> {

}
