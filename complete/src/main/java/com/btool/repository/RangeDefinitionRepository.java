package com.btool.repository;

import com.btool.domain.RangeDefinition;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RangeDefinition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RangeDefinitionRepository extends JpaRepository<RangeDefinition, Long> {

}
