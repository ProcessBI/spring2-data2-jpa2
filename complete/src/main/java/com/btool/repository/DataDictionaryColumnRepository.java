package com.btool.repository;

import com.btool.domain.DataDictionaryColumn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DataDictionaryColumn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataDictionaryColumnRepository extends JpaRepository<DataDictionaryColumn, Long> {

}
