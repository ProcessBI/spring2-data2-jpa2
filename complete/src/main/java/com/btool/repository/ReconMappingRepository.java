package com.btool.repository;

import com.btool.domain.ReconMapping;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconMappingRepository extends JpaRepository<ReconMapping, Long> {

}
