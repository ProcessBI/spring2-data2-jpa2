package com.btool.repository;

import com.btool.domain.TrendResults;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TrendResults entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrendResultsRepository extends JpaRepository<TrendResults, Long> {

}
