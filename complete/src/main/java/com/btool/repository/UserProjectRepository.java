package com.btool.repository;

import com.btool.domain.UserProject;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserProject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserProjectRepository extends JpaRepository<UserProject, Long> {

}
