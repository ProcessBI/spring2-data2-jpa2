package com.btool.repository;

import com.btool.domain.DatasetProperties;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DatasetProperties entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatasetPropertiesRepository extends JpaRepository<DatasetProperties, Long> {

}
