package com.btool.repository;

import com.btool.domain.CriticalDataElement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the CriticalDataElement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CriticalDataElementRepository extends JpaRepository<CriticalDataElement, Long> {

    @Query(value = "select distinct critical_data_element from CriticalDataElement critical_data_element left join fetch critical_data_element.datasetCols",
        countQuery = "select count(distinct critical_data_element) from CriticalDataElement critical_data_element")
    Page<CriticalDataElement> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct critical_data_element from CriticalDataElement critical_data_element left join fetch critical_data_element.datasetCols")
    List<CriticalDataElement> findAllWithEagerRelationships();

    @Query("select critical_data_element from CriticalDataElement critical_data_element left join fetch critical_data_element.datasetCols where critical_data_element.id =:id")
    Optional<CriticalDataElement> findOneWithEagerRelationships(@Param("id") Long id);

}
