package com.btool.repository;

import com.btool.domain.ReconRule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconRuleRepository extends JpaRepository<ReconRule, Long> {

}
