package com.btool.repository;

import com.btool.domain.CodeDecode;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CodeDecode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CodeDecodeRepository extends JpaRepository<CodeDecode, Long> {

}
