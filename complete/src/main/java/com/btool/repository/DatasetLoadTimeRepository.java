package com.btool.repository;

import com.btool.domain.DatasetLoadTime;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DatasetLoadTime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatasetLoadTimeRepository extends JpaRepository<DatasetLoadTime, Long> {

}
