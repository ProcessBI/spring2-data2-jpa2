package com.btool.repository;

import com.btool.domain.DataDictionaryTable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DataDictionaryTable entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataDictionaryTableRepository extends JpaRepository<DataDictionaryTable, Long> {

}
