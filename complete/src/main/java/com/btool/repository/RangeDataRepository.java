package com.btool.repository;

import com.btool.domain.RangeData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RangeData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RangeDataRepository extends JpaRepository<RangeData, Long> {

}
