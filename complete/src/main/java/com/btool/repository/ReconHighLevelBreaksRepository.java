package com.btool.repository;

import com.btool.domain.ReconHighLevelBreaks;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReconHighLevelBreaks entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReconHighLevelBreaksRepository extends JpaRepository<ReconHighLevelBreaks, Long> {

}
