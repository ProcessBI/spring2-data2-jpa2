package com.btool.repository;

import com.btool.domain.ReferenceDataDefinition;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReferenceDataDefinition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReferenceDataDefinitionRepository extends JpaRepository<ReferenceDataDefinition, Long> {

}
