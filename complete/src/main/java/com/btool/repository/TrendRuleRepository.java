package com.btool.repository;

import com.btool.domain.TrendRule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TrendRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrendRuleRepository extends JpaRepository<TrendRule, Long> {

}
