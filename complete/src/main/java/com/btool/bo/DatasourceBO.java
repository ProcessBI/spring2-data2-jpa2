package com.btool.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.btool.domain.Datasource;
import com.btool.repository.DatasourceRepository;

@Service
@Transactional
public class DatasourceBO {
	
	private static final Logger log = LoggerFactory.getLogger(DatasourceBO.class);
	
	@Autowired
	private DatasourceRepository repository;
	
	@Transactional(readOnly=true)
	public void showDatasources() {
		for (Datasource datasource : repository.findAll()) {
			log.info(datasource.toString());
			/*
			 * if(datasource.getDatasourceProperties() != null) {
			 * log.info(datasource.getDatasourceProperties().toString()); }
			 */
		}
	}
}
