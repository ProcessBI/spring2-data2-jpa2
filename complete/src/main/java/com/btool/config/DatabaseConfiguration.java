package com.btool.config;

import java.util.Arrays;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories("com.btool.repository")
//@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
public class DatabaseConfiguration implements EnvironmentAware {

	private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);
	private Environment env;

	@Override
	public void setEnvironment(Environment env) {
		this.env = env;
	}

	@Bean(destroyMethod = "close")
	public DataSource dataSource() {

		log.debug("Configuring Datasource");

		

		HikariConfig config = new HikariConfig();
		config.setDataSourceClassName("com.zaxxer.hikari.HikariDataSource");
		config.addDataSourceProperty("jdbcUrl", "jdbc:mariadb://localhost:3306/btoolv2?useLegacyDatetimeCode=false&serverTimezone=Asia/Calcutta");
		config.addDataSourceProperty("username", "root");
		config.addDataSourceProperty("password", "root");

//			config.addDataSourceProperty("cachePrepStmts", databaseProperties.getHikari().getDataSourceProperties().isCachePrepStmts());
//			config.addDataSourceProperty("prepStmtCacheSize", databaseProperties.getHikari().getDataSourceProperties().getPrepStmtCacheSize());
//			config.addDataSourceProperty("prepStmtCacheSqlLimit", databaseProperties.getHikari().getDataSourceProperties().getPrepStmtCacheSqlLimit());
//			config.addDataSourceProperty("useServerPrepStmts", databaseProperties.getHikari().getDataSourceProperties().isUseServerPrepStmts());


		return new HikariDataSource(config);
	}

}
