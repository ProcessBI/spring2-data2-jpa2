package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.btool.domain.enumeration.RangeType;

/**
 * A RangeDefinition.
 */
@Entity
@Table(name = "td_range_definition")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RangeDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "range_name", length = 100)
    private String rangeName;

    @Enumerated(EnumType.STRING)
    @Column(name = "range_type")
    private RangeType rangeType;

    @OneToMany(mappedBy = "rangeDefinition")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<RangeData> rangeData = new HashSet<>();
    @OneToMany(mappedBy = "rangeDefinition")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconMappingRules> reconMappingRules = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRangeName() {
        return rangeName;
    }

    public RangeDefinition rangeName(String rangeName) {
        this.rangeName = rangeName;
        return this;
    }

    public void setRangeName(String rangeName) {
        this.rangeName = rangeName;
    }

    public RangeType getRangeType() {
        return rangeType;
    }

    public RangeDefinition rangeType(RangeType rangeType) {
        this.rangeType = rangeType;
        return this;
    }

    public void setRangeType(RangeType rangeType) {
        this.rangeType = rangeType;
    }

    public Set<RangeData> getRangeData() {
        return rangeData;
    }

    public RangeDefinition rangeData(Set<RangeData> rangeData) {
        this.rangeData = rangeData;
        return this;
    }

    public RangeDefinition addRangeData(RangeData rangeData) {
        this.rangeData.add(rangeData);
        rangeData.setRangeDefinition(this);
        return this;
    }

    public RangeDefinition removeRangeData(RangeData rangeData) {
        this.rangeData.remove(rangeData);
        rangeData.setRangeDefinition(null);
        return this;
    }

    public void setRangeData(Set<RangeData> rangeData) {
        this.rangeData = rangeData;
    }

    public Set<ReconMappingRules> getReconMappingRules() {
        return reconMappingRules;
    }

    public RangeDefinition reconMappingRules(Set<ReconMappingRules> reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
        return this;
    }

    public RangeDefinition addReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules.add(reconMappingRules);
        reconMappingRules.setRangeDefinition(this);
        return this;
    }

    public RangeDefinition removeReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules.remove(reconMappingRules);
        reconMappingRules.setRangeDefinition(null);
        return this;
    }

    public void setReconMappingRules(Set<ReconMappingRules> reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RangeDefinition rangeDefinition = (RangeDefinition) o;
        if (rangeDefinition.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rangeDefinition.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RangeDefinition{" +
            "id=" + getId() +
            ", rangeName='" + getRangeName() + "'" +
            ", rangeType='" + getRangeType() + "'" +
            "}";
    }
}
