package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ReconMappingRules.
 */
@Entity
@Table(name = "td_recon_mapping_rules")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconMappingRules implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "rule_json")
    private String ruleJson;

    @Column(name = "tolerance")
    private Float tolerance;

    @ManyToOne
    @JsonIgnoreProperties("reconMappingRules")
    private ReconMapping reconMapping;

    @OneToMany(mappedBy = "reconMappingRules")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconResults> reconResults = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("reconMappingRules")
    private ReconRule reconRule;

    @ManyToOne
    @JsonIgnoreProperties("reconMappingRules")
    private RangeDefinition rangeDefinition;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public ReconMappingRules isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getRuleJson() {
        return ruleJson;
    }

    public ReconMappingRules ruleJson(String ruleJson) {
        this.ruleJson = ruleJson;
        return this;
    }

    public void setRuleJson(String ruleJson) {
        this.ruleJson = ruleJson;
    }

    public Float getTolerance() {
        return tolerance;
    }

    public ReconMappingRules tolerance(Float tolerance) {
        this.tolerance = tolerance;
        return this;
    }

    public void setTolerance(Float tolerance) {
        this.tolerance = tolerance;
    }

    public ReconMapping getReconMapping() {
        return reconMapping;
    }

    public ReconMappingRules reconMapping(ReconMapping reconMapping) {
        this.reconMapping = reconMapping;
        return this;
    }

    public void setReconMapping(ReconMapping reconMapping) {
        this.reconMapping = reconMapping;
    }

    public Set<ReconResults> getReconResults() {
        return reconResults;
    }

    public ReconMappingRules reconResults(Set<ReconResults> reconResults) {
        this.reconResults = reconResults;
        return this;
    }

    public ReconMappingRules addReconResults(ReconResults reconResults) {
        this.reconResults.add(reconResults);
        reconResults.setReconMappingRules(this);
        return this;
    }

    public ReconMappingRules removeReconResults(ReconResults reconResults) {
        this.reconResults.remove(reconResults);
        reconResults.setReconMappingRules(null);
        return this;
    }

    public void setReconResults(Set<ReconResults> reconResults) {
        this.reconResults = reconResults;
    }

    public ReconRule getReconRule() {
        return reconRule;
    }

    public ReconMappingRules reconRule(ReconRule reconRule) {
        this.reconRule = reconRule;
        return this;
    }

    public void setReconRule(ReconRule reconRule) {
        this.reconRule = reconRule;
    }

    public RangeDefinition getRangeDefinition() {
        return rangeDefinition;
    }

    public ReconMappingRules rangeDefinition(RangeDefinition rangeDefinition) {
        this.rangeDefinition = rangeDefinition;
        return this;
    }

    public void setRangeDefinition(RangeDefinition rangeDefinition) {
        this.rangeDefinition = rangeDefinition;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconMappingRules reconMappingRules = (ReconMappingRules) o;
        if (reconMappingRules.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconMappingRules.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconMappingRules{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", ruleJson='" + getRuleJson() + "'" +
            ", tolerance=" + getTolerance() +
            "}";
    }
}
