package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ReconRule.
 */
@Entity
@Table(name = "td_recon_rule")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "group_name", length = 30, nullable = false)
    private String groupName;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sub_group", length = 50, nullable = false)
    private String subGroup;

    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "rule_name", length = 250, nullable = false)
    private String ruleName;

    @Column(name = "is_attribute_config")
    private Boolean isAttributeConfig;

    @Column(name = "is_kpi_metrics")
    private Boolean isKpiMetrics;

    @OneToMany(mappedBy = "reconRule")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconMappingRules> reconMappingRules = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public ReconRule isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getGroupName() {
        return groupName;
    }

    public ReconRule groupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSubGroup() {
        return subGroup;
    }

    public ReconRule subGroup(String subGroup) {
        this.subGroup = subGroup;
        return this;
    }

    public void setSubGroup(String subGroup) {
        this.subGroup = subGroup;
    }

    public String getRuleName() {
        return ruleName;
    }

    public ReconRule ruleName(String ruleName) {
        this.ruleName = ruleName;
        return this;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Boolean isIsAttributeConfig() {
        return isAttributeConfig;
    }

    public ReconRule isAttributeConfig(Boolean isAttributeConfig) {
        this.isAttributeConfig = isAttributeConfig;
        return this;
    }

    public void setIsAttributeConfig(Boolean isAttributeConfig) {
        this.isAttributeConfig = isAttributeConfig;
    }

    public Boolean isIsKpiMetrics() {
        return isKpiMetrics;
    }

    public ReconRule isKpiMetrics(Boolean isKpiMetrics) {
        this.isKpiMetrics = isKpiMetrics;
        return this;
    }

    public void setIsKpiMetrics(Boolean isKpiMetrics) {
        this.isKpiMetrics = isKpiMetrics;
    }

    public Set<ReconMappingRules> getReconMappingRules() {
        return reconMappingRules;
    }

    public ReconRule reconMappingRules(Set<ReconMappingRules> reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
        return this;
    }

    public ReconRule addReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules.add(reconMappingRules);
        reconMappingRules.setReconRule(this);
        return this;
    }

    public ReconRule removeReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules.remove(reconMappingRules);
        reconMappingRules.setReconRule(null);
        return this;
    }

    public void setReconMappingRules(Set<ReconMappingRules> reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconRule reconRule = (ReconRule) o;
        if (reconRule.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconRule.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconRule{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", groupName='" + getGroupName() + "'" +
            ", subGroup='" + getSubGroup() + "'" +
            ", ruleName='" + getRuleName() + "'" +
            ", isAttributeConfig='" + isIsAttributeConfig() + "'" +
            ", isKpiMetrics='" + isIsKpiMetrics() + "'" +
            "}";
    }
}
