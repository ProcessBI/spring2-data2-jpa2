package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.btool.domain.enumeration.RunStatus;

/**
 * A RunHistory.
 */
@Entity
@Table(name = "td_run_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RunHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "business_date")
    private LocalDate businessDate;

    @Column(name = "run_date")
    private LocalDate runDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "data_quality_status")
    private RunStatus dataQualityStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "trend_status")
    private RunStatus trendStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "recon_status")
    private RunStatus reconStatus;

    @Size(max = 50)
    @Column(name = "oozie_workflow_id", length = 50)
    private String oozieWorkflowId;

    @Size(max = 50)
    @Column(name = "oozie_application_id", length = 50)
    private String oozieApplicationId;

    @Size(max = 50)
    @Column(name = "spark_application_id", length = 50)
    private String sparkApplicationId;

    @Column(name = "run_locally")
    private Boolean runLocally;

    @OneToMany(mappedBy = "runHistory")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataQualityResults> dataQualityResults = new HashSet<>();
    @OneToMany(mappedBy = "runHistory")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TrendResults> trendResults = new HashSet<>();
    @OneToMany(mappedBy = "runHistory")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconResults> reconResults = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("runHistories")
    private Dataset dataset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getBusinessDate() {
        return businessDate;
    }

    public RunHistory businessDate(LocalDate businessDate) {
        this.businessDate = businessDate;
        return this;
    }

    public void setBusinessDate(LocalDate businessDate) {
        this.businessDate = businessDate;
    }

    public LocalDate getRunDate() {
        return runDate;
    }

    public RunHistory runDate(LocalDate runDate) {
        this.runDate = runDate;
        return this;
    }

    public void setRunDate(LocalDate runDate) {
        this.runDate = runDate;
    }

    public RunStatus getDataQualityStatus() {
        return dataQualityStatus;
    }

    public RunHistory dataQualityStatus(RunStatus dataQualityStatus) {
        this.dataQualityStatus = dataQualityStatus;
        return this;
    }

    public void setDataQualityStatus(RunStatus dataQualityStatus) {
        this.dataQualityStatus = dataQualityStatus;
    }

    public RunStatus getTrendStatus() {
        return trendStatus;
    }

    public RunHistory trendStatus(RunStatus trendStatus) {
        this.trendStatus = trendStatus;
        return this;
    }

    public void setTrendStatus(RunStatus trendStatus) {
        this.trendStatus = trendStatus;
    }

    public RunStatus getReconStatus() {
        return reconStatus;
    }

    public RunHistory reconStatus(RunStatus reconStatus) {
        this.reconStatus = reconStatus;
        return this;
    }

    public void setReconStatus(RunStatus reconStatus) {
        this.reconStatus = reconStatus;
    }

    public String getOozieWorkflowId() {
        return oozieWorkflowId;
    }

    public RunHistory oozieWorkflowId(String oozieWorkflowId) {
        this.oozieWorkflowId = oozieWorkflowId;
        return this;
    }

    public void setOozieWorkflowId(String oozieWorkflowId) {
        this.oozieWorkflowId = oozieWorkflowId;
    }

    public String getOozieApplicationId() {
        return oozieApplicationId;
    }

    public RunHistory oozieApplicationId(String oozieApplicationId) {
        this.oozieApplicationId = oozieApplicationId;
        return this;
    }

    public void setOozieApplicationId(String oozieApplicationId) {
        this.oozieApplicationId = oozieApplicationId;
    }

    public String getSparkApplicationId() {
        return sparkApplicationId;
    }

    public RunHistory sparkApplicationId(String sparkApplicationId) {
        this.sparkApplicationId = sparkApplicationId;
        return this;
    }

    public void setSparkApplicationId(String sparkApplicationId) {
        this.sparkApplicationId = sparkApplicationId;
    }

    public Boolean isRunLocally() {
        return runLocally;
    }

    public RunHistory runLocally(Boolean runLocally) {
        this.runLocally = runLocally;
        return this;
    }

    public void setRunLocally(Boolean runLocally) {
        this.runLocally = runLocally;
    }

    public Set<DataQualityResults> getDataQualityResults() {
        return dataQualityResults;
    }

    public RunHistory dataQualityResults(Set<DataQualityResults> dataQualityResults) {
        this.dataQualityResults = dataQualityResults;
        return this;
    }

    public RunHistory addDataQualityResults(DataQualityResults dataQualityResults) {
        this.dataQualityResults.add(dataQualityResults);
        dataQualityResults.setRunHistory(this);
        return this;
    }

    public RunHistory removeDataQualityResults(DataQualityResults dataQualityResults) {
        this.dataQualityResults.remove(dataQualityResults);
        dataQualityResults.setRunHistory(null);
        return this;
    }

    public void setDataQualityResults(Set<DataQualityResults> dataQualityResults) {
        this.dataQualityResults = dataQualityResults;
    }

    public Set<TrendResults> getTrendResults() {
        return trendResults;
    }

    public RunHistory trendResults(Set<TrendResults> trendResults) {
        this.trendResults = trendResults;
        return this;
    }

    public RunHistory addTrendResults(TrendResults trendResults) {
        this.trendResults.add(trendResults);
        trendResults.setRunHistory(this);
        return this;
    }

    public RunHistory removeTrendResults(TrendResults trendResults) {
        this.trendResults.remove(trendResults);
        trendResults.setRunHistory(null);
        return this;
    }

    public void setTrendResults(Set<TrendResults> trendResults) {
        this.trendResults = trendResults;
    }

    public Set<ReconResults> getReconResults() {
        return reconResults;
    }

    public RunHistory reconResults(Set<ReconResults> reconResults) {
        this.reconResults = reconResults;
        return this;
    }

    public RunHistory addReconResults(ReconResults reconResults) {
        this.reconResults.add(reconResults);
        reconResults.setRunHistory(this);
        return this;
    }

    public RunHistory removeReconResults(ReconResults reconResults) {
        this.reconResults.remove(reconResults);
        reconResults.setRunHistory(null);
        return this;
    }

    public void setReconResults(Set<ReconResults> reconResults) {
        this.reconResults = reconResults;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public RunHistory dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RunHistory runHistory = (RunHistory) o;
        if (runHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), runHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RunHistory{" +
            "id=" + getId() +
            ", businessDate='" + getBusinessDate() + "'" +
            ", runDate='" + getRunDate() + "'" +
            ", dataQualityStatus='" + getDataQualityStatus() + "'" +
            ", trendStatus='" + getTrendStatus() + "'" +
            ", reconStatus='" + getReconStatus() + "'" +
            ", oozieWorkflowId='" + getOozieWorkflowId() + "'" +
            ", oozieApplicationId='" + getOozieApplicationId() + "'" +
            ", sparkApplicationId='" + getSparkApplicationId() + "'" +
            ", runLocally='" + isRunLocally() + "'" +
            "}";
    }
}
