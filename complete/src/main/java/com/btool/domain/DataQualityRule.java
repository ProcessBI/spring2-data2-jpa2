package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DataQualityRule.
 */
@Entity
@Table(name = "td_data_quality_rule")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataQualityRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "group_name", length = 30, nullable = false)
    private String groupName;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sub_group", length = 50, nullable = false)
    private String subGroup;

    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "rule_name", length = 250, nullable = false)
    private String ruleName;

    @Column(name = "is_attribute_config")
    private Boolean isAttributeConfig;

    @OneToMany(mappedBy = "dataQualityRule")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataQualityConfig> dataQualityConfigs = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public DataQualityRule isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getGroupName() {
        return groupName;
    }

    public DataQualityRule groupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSubGroup() {
        return subGroup;
    }

    public DataQualityRule subGroup(String subGroup) {
        this.subGroup = subGroup;
        return this;
    }

    public void setSubGroup(String subGroup) {
        this.subGroup = subGroup;
    }

    public String getRuleName() {
        return ruleName;
    }

    public DataQualityRule ruleName(String ruleName) {
        this.ruleName = ruleName;
        return this;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Boolean isIsAttributeConfig() {
        return isAttributeConfig;
    }

    public DataQualityRule isAttributeConfig(Boolean isAttributeConfig) {
        this.isAttributeConfig = isAttributeConfig;
        return this;
    }

    public void setIsAttributeConfig(Boolean isAttributeConfig) {
        this.isAttributeConfig = isAttributeConfig;
    }

    public Set<DataQualityConfig> getDataQualityConfigs() {
        return dataQualityConfigs;
    }

    public DataQualityRule dataQualityConfigs(Set<DataQualityConfig> dataQualityConfigs) {
        this.dataQualityConfigs = dataQualityConfigs;
        return this;
    }

    public DataQualityRule addDataQualityConfig(DataQualityConfig dataQualityConfig) {
        this.dataQualityConfigs.add(dataQualityConfig);
        dataQualityConfig.setDataQualityRule(this);
        return this;
    }

    public DataQualityRule removeDataQualityConfig(DataQualityConfig dataQualityConfig) {
        this.dataQualityConfigs.remove(dataQualityConfig);
        dataQualityConfig.setDataQualityRule(null);
        return this;
    }

    public void setDataQualityConfigs(Set<DataQualityConfig> dataQualityConfigs) {
        this.dataQualityConfigs = dataQualityConfigs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataQualityRule dataQualityRule = (DataQualityRule) o;
        if (dataQualityRule.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataQualityRule.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataQualityRule{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", groupName='" + getGroupName() + "'" +
            ", subGroup='" + getSubGroup() + "'" +
            ", ruleName='" + getRuleName() + "'" +
            ", isAttributeConfig='" + isIsAttributeConfig() + "'" +
            "}";
    }
}
