package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.btool.domain.enumeration.RuleStatus;

/**
 * A DatasetLoadTime.
 */
@Entity
@Table(name = "td_dataset_load_time")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DatasetLoadTime implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "business_date")
    private LocalDate businessDate;

    @Column(name = "load_time_stamp")
    private ZonedDateTime loadTimeStamp;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RuleStatus status;

    @ManyToOne
    @JsonIgnoreProperties("datasetLoadTimes")
    private Dataset dataset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getBusinessDate() {
        return businessDate;
    }

    public DatasetLoadTime businessDate(LocalDate businessDate) {
        this.businessDate = businessDate;
        return this;
    }

    public void setBusinessDate(LocalDate businessDate) {
        this.businessDate = businessDate;
    }

    public ZonedDateTime getLoadTimeStamp() {
        return loadTimeStamp;
    }

    public DatasetLoadTime loadTimeStamp(ZonedDateTime loadTimeStamp) {
        this.loadTimeStamp = loadTimeStamp;
        return this;
    }

    public void setLoadTimeStamp(ZonedDateTime loadTimeStamp) {
        this.loadTimeStamp = loadTimeStamp;
    }

    public RuleStatus getStatus() {
        return status;
    }

    public DatasetLoadTime status(RuleStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(RuleStatus status) {
        this.status = status;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public DatasetLoadTime dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatasetLoadTime datasetLoadTime = (DatasetLoadTime) o;
        if (datasetLoadTime.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasetLoadTime.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatasetLoadTime{" +
            "id=" + getId() +
            ", businessDate='" + getBusinessDate() + "'" +
            ", loadTimeStamp='" + getLoadTimeStamp() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
