package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DatasetProperties.
 */
@Entity
@Table(name = "td_dataset_properties")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DatasetProperties implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "prop_name", length = 50, nullable = false)
    private String propName;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "prop_value", length = 50, nullable = false)
    private String propValue;

    @ManyToOne
    @JsonIgnoreProperties("datasetProperties")
    private Dataset dataset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPropName() {
        return propName;
    }

    public DatasetProperties propName(String propName) {
        this.propName = propName;
        return this;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public String getPropValue() {
        return propValue;
    }

    public DatasetProperties propValue(String propValue) {
        this.propValue = propValue;
        return this;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public DatasetProperties dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatasetProperties datasetProperties = (DatasetProperties) o;
        if (datasetProperties.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasetProperties.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatasetProperties{" +
            "id=" + getId() +
            ", propName='" + getPropName() + "'" +
            ", propValue='" + getPropValue() + "'" +
            "}";
    }
}
