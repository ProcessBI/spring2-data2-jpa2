package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import com.btool.domain.enumeration.DataDictionaryStatus;

/**
 * A DataDictionaryColumn.
 */
@Entity
@Table(name = "td_data_dictionary_column")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataDictionaryColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "column_name", length = 100)
    private String columnName;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DataDictionaryStatus status;

    @Size(max = 1000)
    @Column(name = "column_description", length = 1000)
    private String columnDescription;

    @Size(max = 10000)
    @Column(name = "derived_description", length = 10000)
    private String derivedDescription;

    @OneToOne    @JoinColumn(unique = true)
    private DatasetColumn datasetColumn;

    @ManyToOne
    @JsonIgnoreProperties("dataDictionaryColumns")
    private DataDictionaryTable dataDictionaryTable;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColumnName() {
        return columnName;
    }

    public DataDictionaryColumn columnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public DataDictionaryStatus getStatus() {
        return status;
    }

    public DataDictionaryColumn status(DataDictionaryStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(DataDictionaryStatus status) {
        this.status = status;
    }

    public String getColumnDescription() {
        return columnDescription;
    }

    public DataDictionaryColumn columnDescription(String columnDescription) {
        this.columnDescription = columnDescription;
        return this;
    }

    public void setColumnDescription(String columnDescription) {
        this.columnDescription = columnDescription;
    }

    public String getDerivedDescription() {
        return derivedDescription;
    }

    public DataDictionaryColumn derivedDescription(String derivedDescription) {
        this.derivedDescription = derivedDescription;
        return this;
    }

    public void setDerivedDescription(String derivedDescription) {
        this.derivedDescription = derivedDescription;
    }

    public DatasetColumn getDatasetColumn() {
        return datasetColumn;
    }

    public DataDictionaryColumn datasetColumn(DatasetColumn datasetColumn) {
        this.datasetColumn = datasetColumn;
        return this;
    }

    public void setDatasetColumn(DatasetColumn datasetColumn) {
        this.datasetColumn = datasetColumn;
    }

    public DataDictionaryTable getDataDictionaryTable() {
        return dataDictionaryTable;
    }

    public DataDictionaryColumn dataDictionaryTable(DataDictionaryTable dataDictionaryTable) {
        this.dataDictionaryTable = dataDictionaryTable;
        return this;
    }

    public void setDataDictionaryTable(DataDictionaryTable dataDictionaryTable) {
        this.dataDictionaryTable = dataDictionaryTable;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataDictionaryColumn dataDictionaryColumn = (DataDictionaryColumn) o;
        if (dataDictionaryColumn.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataDictionaryColumn.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataDictionaryColumn{" +
            "id=" + getId() +
            ", columnName='" + getColumnName() + "'" +
            ", status='" + getStatus() + "'" +
            ", columnDescription='" + getColumnDescription() + "'" +
            ", derivedDescription='" + getDerivedDescription() + "'" +
            "}";
    }
}
