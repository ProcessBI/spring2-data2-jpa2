package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ReconMapping.
 */
@Entity
@Table(name = "td_recon_mapping")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "key_json")
    private String keyJson;

    @OneToMany(mappedBy = "reconMapping")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconMappingRules> reconMappingRules = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("sourceReconMappings")
    private Dataset sourceDataset;

    @ManyToOne
    @JsonIgnoreProperties("reconMappings")
    private Dataset targetDataset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public ReconMapping isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getKeyJson() {
        return keyJson;
    }

    public ReconMapping keyJson(String keyJson) {
        this.keyJson = keyJson;
        return this;
    }

    public void setKeyJson(String keyJson) {
        this.keyJson = keyJson;
    }

    public Set<ReconMappingRules> getReconMappingRules() {
        return reconMappingRules;
    }

    public ReconMapping reconMappingRules(Set<ReconMappingRules> reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
        return this;
    }

    public ReconMapping addReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules.add(reconMappingRules);
        reconMappingRules.setReconMapping(this);
        return this;
    }

    public ReconMapping removeReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules.remove(reconMappingRules);
        reconMappingRules.setReconMapping(null);
        return this;
    }

    public void setReconMappingRules(Set<ReconMappingRules> reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
    }

    public Dataset getSourceDataset() {
        return sourceDataset;
    }

    public ReconMapping sourceDataset(Dataset dataset) {
        this.sourceDataset = dataset;
        return this;
    }

    public void setSourceDataset(Dataset dataset) {
        this.sourceDataset = dataset;
    }

    public Dataset getTargetDataset() {
        return targetDataset;
    }

    public ReconMapping targetDataset(Dataset dataset) {
        this.targetDataset = dataset;
        return this;
    }

    public void setTargetDataset(Dataset dataset) {
        this.targetDataset = dataset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconMapping reconMapping = (ReconMapping) o;
        if (reconMapping.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconMapping.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconMapping{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", keyJson='" + getKeyJson() + "'" +
            "}";
    }
}
