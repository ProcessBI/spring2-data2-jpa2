package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.btool.domain.enumeration.RuleStatus;

/**
 * A ReconResults.
 */
@Entity
@Table(name = "td_recon_results")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconResults implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RuleStatus status;

    @Column(name = "key_json")
    private String keyJson;

    @Column(name = "summary_json")
    private String summaryJson;

    @Size(max = 500)
    @Column(name = "error_string", length = 500)
    private String errorString;

    @ManyToOne
    @JsonIgnoreProperties("reconResults")
    private ReconMappingRules reconMappingRules;

    @ManyToOne
    @JsonIgnoreProperties("reconResults")
    private RunHistory runHistory;

    @OneToMany(mappedBy = "reconResults")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconHighLevelBreaks> reconHighLevelBreaks = new HashSet<>();
    @OneToMany(mappedBy = "reconResults")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconAttributeResults> reconAttributeResults = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RuleStatus getStatus() {
        return status;
    }

    public ReconResults status(RuleStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(RuleStatus status) {
        this.status = status;
    }

    public String getKeyJson() {
        return keyJson;
    }

    public ReconResults keyJson(String keyJson) {
        this.keyJson = keyJson;
        return this;
    }

    public void setKeyJson(String keyJson) {
        this.keyJson = keyJson;
    }

    public String getSummaryJson() {
        return summaryJson;
    }

    public ReconResults summaryJson(String summaryJson) {
        this.summaryJson = summaryJson;
        return this;
    }

    public void setSummaryJson(String summaryJson) {
        this.summaryJson = summaryJson;
    }

    public String getErrorString() {
        return errorString;
    }

    public ReconResults errorString(String errorString) {
        this.errorString = errorString;
        return this;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

    public ReconMappingRules getReconMappingRules() {
        return reconMappingRules;
    }

    public ReconResults reconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
        return this;
    }

    public void setReconMappingRules(ReconMappingRules reconMappingRules) {
        this.reconMappingRules = reconMappingRules;
    }

    public RunHistory getRunHistory() {
        return runHistory;
    }

    public ReconResults runHistory(RunHistory runHistory) {
        this.runHistory = runHistory;
        return this;
    }

    public void setRunHistory(RunHistory runHistory) {
        this.runHistory = runHistory;
    }

    public Set<ReconHighLevelBreaks> getReconHighLevelBreaks() {
        return reconHighLevelBreaks;
    }

    public ReconResults reconHighLevelBreaks(Set<ReconHighLevelBreaks> reconHighLevelBreaks) {
        this.reconHighLevelBreaks = reconHighLevelBreaks;
        return this;
    }

    public ReconResults addReconHighLevelBreaks(ReconHighLevelBreaks reconHighLevelBreaks) {
        this.reconHighLevelBreaks.add(reconHighLevelBreaks);
        reconHighLevelBreaks.setReconResults(this);
        return this;
    }

    public ReconResults removeReconHighLevelBreaks(ReconHighLevelBreaks reconHighLevelBreaks) {
        this.reconHighLevelBreaks.remove(reconHighLevelBreaks);
        reconHighLevelBreaks.setReconResults(null);
        return this;
    }

    public void setReconHighLevelBreaks(Set<ReconHighLevelBreaks> reconHighLevelBreaks) {
        this.reconHighLevelBreaks = reconHighLevelBreaks;
    }

    public Set<ReconAttributeResults> getReconAttributeResults() {
        return reconAttributeResults;
    }

    public ReconResults reconAttributeResults(Set<ReconAttributeResults> reconAttributeResults) {
        this.reconAttributeResults = reconAttributeResults;
        return this;
    }

    public ReconResults addReconAttributeResults(ReconAttributeResults reconAttributeResults) {
        this.reconAttributeResults.add(reconAttributeResults);
        reconAttributeResults.setReconResults(this);
        return this;
    }

    public ReconResults removeReconAttributeResults(ReconAttributeResults reconAttributeResults) {
        this.reconAttributeResults.remove(reconAttributeResults);
        reconAttributeResults.setReconResults(null);
        return this;
    }

    public void setReconAttributeResults(Set<ReconAttributeResults> reconAttributeResults) {
        this.reconAttributeResults = reconAttributeResults;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconResults reconResults = (ReconResults) o;
        if (reconResults.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconResults.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconResults{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", keyJson='" + getKeyJson() + "'" +
            ", summaryJson='" + getSummaryJson() + "'" +
            ", errorString='" + getErrorString() + "'" +
            "}";
    }
}
