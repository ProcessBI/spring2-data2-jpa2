package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DataQualityResults.
 */
@Entity
@Table(name = "td_data_quality_results")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataQualityResults implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "status", nullable = false)
    private Integer status;

    @Column(name = "total_count")
    private Integer totalCount;

    @Column(name = "exception_count")
    private Integer exceptionCount;

    @Column(name = "calc_value")
    private Integer calcValue;

    @Size(max = 200)
    @Column(name = "dataset_key", length = 200)
    private String datasetKey;

    @Column(name = "result_json")
    private String resultJson;

    @ManyToOne
    @JsonIgnoreProperties("dataQualityResults")
    private DataQualityConfig dataQualityConfig;

    @ManyToOne
    @JsonIgnoreProperties("dataQualityResults")
    private RunHistory runHistory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public DataQualityResults status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public DataQualityResults totalCount(Integer totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getExceptionCount() {
        return exceptionCount;
    }

    public DataQualityResults exceptionCount(Integer exceptionCount) {
        this.exceptionCount = exceptionCount;
        return this;
    }

    public void setExceptionCount(Integer exceptionCount) {
        this.exceptionCount = exceptionCount;
    }

    public Integer getCalcValue() {
        return calcValue;
    }

    public DataQualityResults calcValue(Integer calcValue) {
        this.calcValue = calcValue;
        return this;
    }

    public void setCalcValue(Integer calcValue) {
        this.calcValue = calcValue;
    }

    public String getDatasetKey() {
        return datasetKey;
    }

    public DataQualityResults datasetKey(String datasetKey) {
        this.datasetKey = datasetKey;
        return this;
    }

    public void setDatasetKey(String datasetKey) {
        this.datasetKey = datasetKey;
    }

    public String getResultJson() {
        return resultJson;
    }

    public DataQualityResults resultJson(String resultJson) {
        this.resultJson = resultJson;
        return this;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    public DataQualityConfig getDataQualityConfig() {
        return dataQualityConfig;
    }

    public DataQualityResults dataQualityConfig(DataQualityConfig dataQualityConfig) {
        this.dataQualityConfig = dataQualityConfig;
        return this;
    }

    public void setDataQualityConfig(DataQualityConfig dataQualityConfig) {
        this.dataQualityConfig = dataQualityConfig;
    }

    public RunHistory getRunHistory() {
        return runHistory;
    }

    public DataQualityResults runHistory(RunHistory runHistory) {
        this.runHistory = runHistory;
        return this;
    }

    public void setRunHistory(RunHistory runHistory) {
        this.runHistory = runHistory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataQualityResults dataQualityResults = (DataQualityResults) o;
        if (dataQualityResults.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataQualityResults.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataQualityResults{" +
            "id=" + getId() +
            ", status=" + getStatus() +
            ", totalCount=" + getTotalCount() +
            ", exceptionCount=" + getExceptionCount() +
            ", calcValue=" + getCalcValue() +
            ", datasetKey='" + getDatasetKey() + "'" +
            ", resultJson='" + getResultJson() + "'" +
            "}";
    }
}
