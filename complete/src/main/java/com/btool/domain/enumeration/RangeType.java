package com.btool.domain.enumeration;

/**
 * The RangeType enumeration.
 */
public enum RangeType {
    NUMRIC_RANGE, DATE_RANGE
}
