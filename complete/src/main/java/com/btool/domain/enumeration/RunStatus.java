package com.btool.domain.enumeration;

/**
 * The RunStatus enumeration.
 */
public enum RunStatus {
    SUBMITTED, RUNNING, SUCCESS, FAILED, ERROR
}
