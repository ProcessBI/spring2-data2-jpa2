package com.btool.domain.enumeration;

/**
 * The TrendDataValueType enumeration.
 */
public enum TrendDataValueType {
    ACTUAL, UPPER_BOUND, LOWER_BOUND
}
