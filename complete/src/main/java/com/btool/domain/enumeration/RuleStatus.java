package com.btool.domain.enumeration;

/**
 * The RuleStatus enumeration.
 */
public enum RuleStatus {
    SUCCESS, FAILED, ERROR
}
