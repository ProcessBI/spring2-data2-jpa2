package com.btool.domain.enumeration;

/**
 * The TrendDataInputType enumeration.
 */
public enum TrendDataInputType {
    USER, SYSTEM
}
