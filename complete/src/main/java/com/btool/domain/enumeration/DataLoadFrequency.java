package com.btool.domain.enumeration;

/**
 * The DataLoadFrequency enumeration.
 */
public enum DataLoadFrequency {
    MONTHLY, WEEKLY, DAILY
}
