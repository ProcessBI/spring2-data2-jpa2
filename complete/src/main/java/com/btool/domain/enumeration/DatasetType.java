package com.btool.domain.enumeration;

/**
 * The DatasetType enumeration.
 */
public enum DatasetType {
    TABLE, VDS, KPI, FILE
}
