package com.btool.domain.enumeration;

/**
 * The DataDictionaryStatus enumeration.
 */
public enum DataDictionaryStatus {
    MATCHED, UNMATCHED
}
