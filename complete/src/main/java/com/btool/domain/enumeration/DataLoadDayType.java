package com.btool.domain.enumeration;

/**
 * The DataLoadDayType enumeration.
 */
public enum DataLoadDayType {
    WORKDAY, WEEKDAY
}
