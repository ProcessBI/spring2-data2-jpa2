package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ReconAttributeLevelBreaks.
 */
@Entity
@Table(name = "td_recon_attribute_level_breaks")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconAttributeLevelBreaks implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "result_json")
    private String resultJson;

    @ManyToOne
    @JsonIgnoreProperties("reconAttributeLevelBreaks")
    private ReconAttributeResults reconAttributeResults;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResultJson() {
        return resultJson;
    }

    public ReconAttributeLevelBreaks resultJson(String resultJson) {
        this.resultJson = resultJson;
        return this;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    public ReconAttributeResults getReconAttributeResults() {
        return reconAttributeResults;
    }

    public ReconAttributeLevelBreaks reconAttributeResults(ReconAttributeResults reconAttributeResults) {
        this.reconAttributeResults = reconAttributeResults;
        return this;
    }

    public void setReconAttributeResults(ReconAttributeResults reconAttributeResults) {
        this.reconAttributeResults = reconAttributeResults;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconAttributeLevelBreaks reconAttributeLevelBreaks = (ReconAttributeLevelBreaks) o;
        if (reconAttributeLevelBreaks.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconAttributeLevelBreaks.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconAttributeLevelBreaks{" +
            "id=" + getId() +
            ", resultJson='" + getResultJson() + "'" +
            "}";
    }
}
