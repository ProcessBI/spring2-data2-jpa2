package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Datasource.
 */
@Entity
@Table(name = "td_datasource")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Datasource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "alias", length = 100, nullable = false)
    private String alias;

    @Size(max = 250)
    @Column(name = "description", length = 250)
    private String description;

    @OneToMany(mappedBy = "datasource")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DatasourceProperties> datasourceProperties = new HashSet<>();
    @OneToMany(mappedBy = "datasource")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Dataset> datasets = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public Datasource isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public Datasource name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public Datasource alias(String alias) {
        this.alias = alias;
        return this;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public Datasource description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<DatasourceProperties> getDatasourceProperties() {
        return datasourceProperties;
    }

    public Datasource datasourceProperties(Set<DatasourceProperties> datasourceProperties) {
        this.datasourceProperties = datasourceProperties;
        return this;
    }

    public Datasource addDatasourceProperties(DatasourceProperties datasourceProperties) {
        this.datasourceProperties.add(datasourceProperties);
        datasourceProperties.setDatasource(this);
        return this;
    }

    public Datasource removeDatasourceProperties(DatasourceProperties datasourceProperties) {
        this.datasourceProperties.remove(datasourceProperties);
        datasourceProperties.setDatasource(null);
        return this;
    }

    public void setDatasourceProperties(Set<DatasourceProperties> datasourceProperties) {
        this.datasourceProperties = datasourceProperties;
    }

    public Set<Dataset> getDatasets() {
        return datasets;
    }

    public Datasource datasets(Set<Dataset> datasets) {
        this.datasets = datasets;
        return this;
    }

    public Datasource addDataset(Dataset dataset) {
        this.datasets.add(dataset);
        dataset.setDatasource(this);
        return this;
    }

    public Datasource removeDataset(Dataset dataset) {
        this.datasets.remove(dataset);
        dataset.setDatasource(null);
        return this;
    }

    public void setDatasets(Set<Dataset> datasets) {
        this.datasets = datasets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Datasource datasource = (Datasource) o;
        if (datasource.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasource.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Datasource{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", name='" + getName() + "'" +
            ", alias='" + getAlias() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
