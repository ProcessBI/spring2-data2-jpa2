package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ReferenceDataDefinition.
 */
@Entity
@Table(name = "td_reference_data_definition")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReferenceDataDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "ref_data_map")
    private String refDataMap;

    @ManyToOne
    @JsonIgnoreProperties("sourceRefDataDefs")
    private Dataset sourceDataset;

    @ManyToOne
    @JsonIgnoreProperties("refDataDefs")
    private Dataset targetDataset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ReferenceDataDefinition name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefDataMap() {
        return refDataMap;
    }

    public ReferenceDataDefinition refDataMap(String refDataMap) {
        this.refDataMap = refDataMap;
        return this;
    }

    public void setRefDataMap(String refDataMap) {
        this.refDataMap = refDataMap;
    }

    public Dataset getSourceDataset() {
        return sourceDataset;
    }

    public ReferenceDataDefinition sourceDataset(Dataset dataset) {
        this.sourceDataset = dataset;
        return this;
    }

    public void setSourceDataset(Dataset dataset) {
        this.sourceDataset = dataset;
    }

    public Dataset getTargetDataset() {
        return targetDataset;
    }

    public ReferenceDataDefinition targetDataset(Dataset dataset) {
        this.targetDataset = dataset;
        return this;
    }

    public void setTargetDataset(Dataset dataset) {
        this.targetDataset = dataset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReferenceDataDefinition referenceDataDefinition = (ReferenceDataDefinition) o;
        if (referenceDataDefinition.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), referenceDataDefinition.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReferenceDataDefinition{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", refDataMap='" + getRefDataMap() + "'" +
            "}";
    }
}
