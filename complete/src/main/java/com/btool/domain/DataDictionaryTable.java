package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.btool.domain.enumeration.DataDictionaryStatus;

/**
 * A DataDictionaryTable.
 */
@Entity
@Table(name = "td_data_dictionary_table")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataDictionaryTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "database_name", length = 100)
    private String databaseName;

    @Size(max = 100)
    @Column(name = "table_name", length = 100)
    private String tableName;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DataDictionaryStatus status;

    @OneToOne    @JoinColumn(unique = true)
    private Dataset dataset;

    @OneToMany(mappedBy = "dataDictionaryTable")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataDictionaryColumn> dataDictionaryColumns = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public DataDictionaryTable databaseName(String databaseName) {
        this.databaseName = databaseName;
        return this;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getTableName() {
        return tableName;
    }

    public DataDictionaryTable tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public DataDictionaryStatus getStatus() {
        return status;
    }

    public DataDictionaryTable status(DataDictionaryStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(DataDictionaryStatus status) {
        this.status = status;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public DataDictionaryTable dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public Set<DataDictionaryColumn> getDataDictionaryColumns() {
        return dataDictionaryColumns;
    }

    public DataDictionaryTable dataDictionaryColumns(Set<DataDictionaryColumn> dataDictionaryColumns) {
        this.dataDictionaryColumns = dataDictionaryColumns;
        return this;
    }

    public DataDictionaryTable addDataDictionaryColumn(DataDictionaryColumn dataDictionaryColumn) {
        this.dataDictionaryColumns.add(dataDictionaryColumn);
        dataDictionaryColumn.setDataDictionaryTable(this);
        return this;
    }

    public DataDictionaryTable removeDataDictionaryColumn(DataDictionaryColumn dataDictionaryColumn) {
        this.dataDictionaryColumns.remove(dataDictionaryColumn);
        dataDictionaryColumn.setDataDictionaryTable(null);
        return this;
    }

    public void setDataDictionaryColumns(Set<DataDictionaryColumn> dataDictionaryColumns) {
        this.dataDictionaryColumns = dataDictionaryColumns;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataDictionaryTable dataDictionaryTable = (DataDictionaryTable) o;
        if (dataDictionaryTable.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataDictionaryTable.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataDictionaryTable{" +
            "id=" + getId() +
            ", databaseName='" + getDatabaseName() + "'" +
            ", tableName='" + getTableName() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
