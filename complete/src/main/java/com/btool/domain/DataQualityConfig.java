package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DataQualityConfig.
 */
@Entity
@Table(name = "td_data_quality_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataQualityConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Size(max = 50)
    @Column(name = "column_name", length = 50)
    private String columnName;

    @Column(name = "rule_json")
    private String ruleJson;

    @Column(name = "tolerance")
    private Float tolerance;

    @ManyToOne
    @JsonIgnoreProperties("dataQualityConfigs")
    private Dataset dataset;

    @OneToMany(mappedBy = "dataQualityConfig")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataQualityResults> dataQualityResults = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("dataQualityConfigs")
    private DataQualityRule dataQualityRule;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public DataQualityConfig isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getColumnName() {
        return columnName;
    }

    public DataQualityConfig columnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getRuleJson() {
        return ruleJson;
    }

    public DataQualityConfig ruleJson(String ruleJson) {
        this.ruleJson = ruleJson;
        return this;
    }

    public void setRuleJson(String ruleJson) {
        this.ruleJson = ruleJson;
    }

    public Float getTolerance() {
        return tolerance;
    }

    public DataQualityConfig tolerance(Float tolerance) {
        this.tolerance = tolerance;
        return this;
    }

    public void setTolerance(Float tolerance) {
        this.tolerance = tolerance;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public DataQualityConfig dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public Set<DataQualityResults> getDataQualityResults() {
        return dataQualityResults;
    }

    public DataQualityConfig dataQualityResults(Set<DataQualityResults> dataQualityResults) {
        this.dataQualityResults = dataQualityResults;
        return this;
    }

    public DataQualityConfig addDataQualityResults(DataQualityResults dataQualityResults) {
        this.dataQualityResults.add(dataQualityResults);
        dataQualityResults.setDataQualityConfig(this);
        return this;
    }

    public DataQualityConfig removeDataQualityResults(DataQualityResults dataQualityResults) {
        this.dataQualityResults.remove(dataQualityResults);
        dataQualityResults.setDataQualityConfig(null);
        return this;
    }

    public void setDataQualityResults(Set<DataQualityResults> dataQualityResults) {
        this.dataQualityResults = dataQualityResults;
    }

    public DataQualityRule getDataQualityRule() {
        return dataQualityRule;
    }

    public DataQualityConfig dataQualityRule(DataQualityRule dataQualityRule) {
        this.dataQualityRule = dataQualityRule;
        return this;
    }

    public void setDataQualityRule(DataQualityRule dataQualityRule) {
        this.dataQualityRule = dataQualityRule;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataQualityConfig dataQualityConfig = (DataQualityConfig) o;
        if (dataQualityConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataQualityConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataQualityConfig{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", columnName='" + getColumnName() + "'" +
            ", ruleJson='" + getRuleJson() + "'" +
            ", tolerance=" + getTolerance() +
            "}";
    }
}
