package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.btool.domain.enumeration.DatasetType;

import com.btool.domain.enumeration.DataLoadDayType;

import com.btool.domain.enumeration.DataLoadFrequency;

/**
 * A Dataset.
 */
@Entity
@Table(name = "td_dataset")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Dataset implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dataset_name", length = 100, nullable = false)
    private String datasetName;

    @Size(max = 100)
    @Column(name = "dataset_alias", length = 100)
    private String datasetAlias;

    @Enumerated(EnumType.STRING)
    @Column(name = "dataset_type")
    private DatasetType datasetType;

    @Enumerated(EnumType.STRING)
    @Column(name = "day_type")
    private DataLoadDayType dayType;

    @Enumerated(EnumType.STRING)
    @Column(name = "data_load_frequency")
    private DataLoadFrequency dataLoadFrequency;

    @Column(name = "sla_day")
    private Integer slaDay;

    @Size(max = 8)
    @Column(name = "sla_time", length = 8)
    private String slaTime;

    @ManyToOne
    @JsonIgnoreProperties("datasets")
    private Datasource datasource;

    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DatasetProperties> datasetProperties = new HashSet<>();
    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DatasetColumn> datasetColumns = new HashSet<>();
    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataQualityConfig> dataQualityConfigs = new HashSet<>();
    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TrendConfig> trendConfigs = new HashSet<>();
    @OneToMany(mappedBy = "sourceDataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconMapping> sourceReconMappings = new HashSet<>();
    @OneToMany(mappedBy = "targetDataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconMapping> reconMappings = new HashSet<>();
    @OneToMany(mappedBy = "sourceDataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReferenceDataDefinition> sourceRefDataDefs = new HashSet<>();
    @OneToMany(mappedBy = "targetDataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReferenceDataDefinition> refDataDefs = new HashSet<>();
    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ProjectDatasetMapping> projectDatasetMappings = new HashSet<>();
    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DatasetLoadTime> datasetLoadTimes = new HashSet<>();
    @OneToMany(mappedBy = "dataset")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<RunHistory> runHistories = new HashSet<>();
    @OneToOne(mappedBy = "dataset")
    @JsonIgnore
    private DataDictionaryTable dataDictionaryTable;

    @ManyToOne
    @JsonIgnoreProperties("datasets")
    private Project project;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public Dataset isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public Dataset datasetName(String datasetName) {
        this.datasetName = datasetName;
        return this;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public String getDatasetAlias() {
        return datasetAlias;
    }

    public Dataset datasetAlias(String datasetAlias) {
        this.datasetAlias = datasetAlias;
        return this;
    }

    public void setDatasetAlias(String datasetAlias) {
        this.datasetAlias = datasetAlias;
    }

    public DatasetType getDatasetType() {
        return datasetType;
    }

    public Dataset datasetType(DatasetType datasetType) {
        this.datasetType = datasetType;
        return this;
    }

    public void setDatasetType(DatasetType datasetType) {
        this.datasetType = datasetType;
    }

    public DataLoadDayType getDayType() {
        return dayType;
    }

    public Dataset dayType(DataLoadDayType dayType) {
        this.dayType = dayType;
        return this;
    }

    public void setDayType(DataLoadDayType dayType) {
        this.dayType = dayType;
    }

    public DataLoadFrequency getDataLoadFrequency() {
        return dataLoadFrequency;
    }

    public Dataset dataLoadFrequency(DataLoadFrequency dataLoadFrequency) {
        this.dataLoadFrequency = dataLoadFrequency;
        return this;
    }

    public void setDataLoadFrequency(DataLoadFrequency dataLoadFrequency) {
        this.dataLoadFrequency = dataLoadFrequency;
    }

    public Integer getSlaDay() {
        return slaDay;
    }

    public Dataset slaDay(Integer slaDay) {
        this.slaDay = slaDay;
        return this;
    }

    public void setSlaDay(Integer slaDay) {
        this.slaDay = slaDay;
    }

    public String getSlaTime() {
        return slaTime;
    }

    public Dataset slaTime(String slaTime) {
        this.slaTime = slaTime;
        return this;
    }

    public void setSlaTime(String slaTime) {
        this.slaTime = slaTime;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public Dataset datasource(Datasource datasource) {
        this.datasource = datasource;
        return this;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    public Set<DatasetProperties> getDatasetProperties() {
        return datasetProperties;
    }

    public Dataset datasetProperties(Set<DatasetProperties> datasetProperties) {
        this.datasetProperties = datasetProperties;
        return this;
    }

    public Dataset addDatasetProperties(DatasetProperties datasetProperties) {
        this.datasetProperties.add(datasetProperties);
        datasetProperties.setDataset(this);
        return this;
    }

    public Dataset removeDatasetProperties(DatasetProperties datasetProperties) {
        this.datasetProperties.remove(datasetProperties);
        datasetProperties.setDataset(null);
        return this;
    }

    public void setDatasetProperties(Set<DatasetProperties> datasetProperties) {
        this.datasetProperties = datasetProperties;
    }

    public Set<DatasetColumn> getDatasetColumns() {
        return datasetColumns;
    }

    public Dataset datasetColumns(Set<DatasetColumn> datasetColumns) {
        this.datasetColumns = datasetColumns;
        return this;
    }

    public Dataset addDatasetColumn(DatasetColumn datasetColumn) {
        this.datasetColumns.add(datasetColumn);
        datasetColumn.setDataset(this);
        return this;
    }

    public Dataset removeDatasetColumn(DatasetColumn datasetColumn) {
        this.datasetColumns.remove(datasetColumn);
        datasetColumn.setDataset(null);
        return this;
    }

    public void setDatasetColumns(Set<DatasetColumn> datasetColumns) {
        this.datasetColumns = datasetColumns;
    }

    public Set<DataQualityConfig> getDataQualityConfigs() {
        return dataQualityConfigs;
    }

    public Dataset dataQualityConfigs(Set<DataQualityConfig> dataQualityConfigs) {
        this.dataQualityConfigs = dataQualityConfigs;
        return this;
    }

    public Dataset addDataQualityConfig(DataQualityConfig dataQualityConfig) {
        this.dataQualityConfigs.add(dataQualityConfig);
        dataQualityConfig.setDataset(this);
        return this;
    }

    public Dataset removeDataQualityConfig(DataQualityConfig dataQualityConfig) {
        this.dataQualityConfigs.remove(dataQualityConfig);
        dataQualityConfig.setDataset(null);
        return this;
    }

    public void setDataQualityConfigs(Set<DataQualityConfig> dataQualityConfigs) {
        this.dataQualityConfigs = dataQualityConfigs;
    }

    public Set<TrendConfig> getTrendConfigs() {
        return trendConfigs;
    }

    public Dataset trendConfigs(Set<TrendConfig> trendConfigs) {
        this.trendConfigs = trendConfigs;
        return this;
    }

    public Dataset addTrendConfig(TrendConfig trendConfig) {
        this.trendConfigs.add(trendConfig);
        trendConfig.setDataset(this);
        return this;
    }

    public Dataset removeTrendConfig(TrendConfig trendConfig) {
        this.trendConfigs.remove(trendConfig);
        trendConfig.setDataset(null);
        return this;
    }

    public void setTrendConfigs(Set<TrendConfig> trendConfigs) {
        this.trendConfigs = trendConfigs;
    }

    public Set<ReconMapping> getSourceReconMappings() {
        return sourceReconMappings;
    }

    public Dataset sourceReconMappings(Set<ReconMapping> reconMappings) {
        this.sourceReconMappings = reconMappings;
        return this;
    }

    public Dataset addSourceReconMapping(ReconMapping reconMapping) {
        this.sourceReconMappings.add(reconMapping);
        reconMapping.setSourceDataset(this);
        return this;
    }

    public Dataset removeSourceReconMapping(ReconMapping reconMapping) {
        this.sourceReconMappings.remove(reconMapping);
        reconMapping.setSourceDataset(null);
        return this;
    }

    public void setSourceReconMappings(Set<ReconMapping> reconMappings) {
        this.sourceReconMappings = reconMappings;
    }

    public Set<ReconMapping> getReconMappings() {
        return reconMappings;
    }

    public Dataset reconMappings(Set<ReconMapping> reconMappings) {
        this.reconMappings = reconMappings;
        return this;
    }

    public Dataset addReconMapping(ReconMapping reconMapping) {
        this.reconMappings.add(reconMapping);
        reconMapping.setTargetDataset(this);
        return this;
    }

    public Dataset removeReconMapping(ReconMapping reconMapping) {
        this.reconMappings.remove(reconMapping);
        reconMapping.setTargetDataset(null);
        return this;
    }

    public void setReconMappings(Set<ReconMapping> reconMappings) {
        this.reconMappings = reconMappings;
    }

    public Set<ReferenceDataDefinition> getSourceRefDataDefs() {
        return sourceRefDataDefs;
    }

    public Dataset sourceRefDataDefs(Set<ReferenceDataDefinition> referenceDataDefinitions) {
        this.sourceRefDataDefs = referenceDataDefinitions;
        return this;
    }

    public Dataset addSourceRefDataDef(ReferenceDataDefinition referenceDataDefinition) {
        this.sourceRefDataDefs.add(referenceDataDefinition);
        referenceDataDefinition.setSourceDataset(this);
        return this;
    }

    public Dataset removeSourceRefDataDef(ReferenceDataDefinition referenceDataDefinition) {
        this.sourceRefDataDefs.remove(referenceDataDefinition);
        referenceDataDefinition.setSourceDataset(null);
        return this;
    }

    public void setSourceRefDataDefs(Set<ReferenceDataDefinition> referenceDataDefinitions) {
        this.sourceRefDataDefs = referenceDataDefinitions;
    }

    public Set<ReferenceDataDefinition> getRefDataDefs() {
        return refDataDefs;
    }

    public Dataset refDataDefs(Set<ReferenceDataDefinition> referenceDataDefinitions) {
        this.refDataDefs = referenceDataDefinitions;
        return this;
    }

    public Dataset addRefDataDef(ReferenceDataDefinition referenceDataDefinition) {
        this.refDataDefs.add(referenceDataDefinition);
        referenceDataDefinition.setTargetDataset(this);
        return this;
    }

    public Dataset removeRefDataDef(ReferenceDataDefinition referenceDataDefinition) {
        this.refDataDefs.remove(referenceDataDefinition);
        referenceDataDefinition.setTargetDataset(null);
        return this;
    }

    public void setRefDataDefs(Set<ReferenceDataDefinition> referenceDataDefinitions) {
        this.refDataDefs = referenceDataDefinitions;
    }

    public Set<ProjectDatasetMapping> getProjectDatasetMappings() {
        return projectDatasetMappings;
    }

    public Dataset projectDatasetMappings(Set<ProjectDatasetMapping> projectDatasetMappings) {
        this.projectDatasetMappings = projectDatasetMappings;
        return this;
    }

    public Dataset addProjectDatasetMapping(ProjectDatasetMapping projectDatasetMapping) {
        this.projectDatasetMappings.add(projectDatasetMapping);
        projectDatasetMapping.setDataset(this);
        return this;
    }

    public Dataset removeProjectDatasetMapping(ProjectDatasetMapping projectDatasetMapping) {
        this.projectDatasetMappings.remove(projectDatasetMapping);
        projectDatasetMapping.setDataset(null);
        return this;
    }

    public void setProjectDatasetMappings(Set<ProjectDatasetMapping> projectDatasetMappings) {
        this.projectDatasetMappings = projectDatasetMappings;
    }

    public Set<DatasetLoadTime> getDatasetLoadTimes() {
        return datasetLoadTimes;
    }

    public Dataset datasetLoadTimes(Set<DatasetLoadTime> datasetLoadTimes) {
        this.datasetLoadTimes = datasetLoadTimes;
        return this;
    }

    public Dataset addDatasetLoadTime(DatasetLoadTime datasetLoadTime) {
        this.datasetLoadTimes.add(datasetLoadTime);
        datasetLoadTime.setDataset(this);
        return this;
    }

    public Dataset removeDatasetLoadTime(DatasetLoadTime datasetLoadTime) {
        this.datasetLoadTimes.remove(datasetLoadTime);
        datasetLoadTime.setDataset(null);
        return this;
    }

    public void setDatasetLoadTimes(Set<DatasetLoadTime> datasetLoadTimes) {
        this.datasetLoadTimes = datasetLoadTimes;
    }

    public Set<RunHistory> getRunHistories() {
        return runHistories;
    }

    public Dataset runHistories(Set<RunHistory> runHistories) {
        this.runHistories = runHistories;
        return this;
    }

    public Dataset addRunHistory(RunHistory runHistory) {
        this.runHistories.add(runHistory);
        runHistory.setDataset(this);
        return this;
    }

    public Dataset removeRunHistory(RunHistory runHistory) {
        this.runHistories.remove(runHistory);
        runHistory.setDataset(null);
        return this;
    }

    public void setRunHistories(Set<RunHistory> runHistories) {
        this.runHistories = runHistories;
    }

    public DataDictionaryTable getDataDictionaryTable() {
        return dataDictionaryTable;
    }

    public Dataset dataDictionaryTable(DataDictionaryTable dataDictionaryTable) {
        this.dataDictionaryTable = dataDictionaryTable;
        return this;
    }

    public void setDataDictionaryTable(DataDictionaryTable dataDictionaryTable) {
        this.dataDictionaryTable = dataDictionaryTable;
    }

    public Project getProject() {
        return project;
    }

    public Dataset project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dataset dataset = (Dataset) o;
        if (dataset.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataset.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Dataset{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", datasetName='" + getDatasetName() + "'" +
            ", datasetAlias='" + getDatasetAlias() + "'" +
            ", datasetType='" + getDatasetType() + "'" +
            ", dayType='" + getDayType() + "'" +
            ", dataLoadFrequency='" + getDataLoadFrequency() + "'" +
            ", slaDay=" + getSlaDay() +
            ", slaTime='" + getSlaTime() + "'" +
            "}";
    }
}
