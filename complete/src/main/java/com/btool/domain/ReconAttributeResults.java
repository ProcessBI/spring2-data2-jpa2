package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ReconAttributeResults.
 */
@Entity
@Table(name = "td_recon_attribute_results")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconAttributeResults implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 250)
    @Column(name = "attribute_value", length = 250)
    private String attributeValue;

    @Column(name = "summary_json")
    private String summaryJson;

    @ManyToOne
    @JsonIgnoreProperties("reconAttributeResults")
    private ReconResults reconResults;

    @OneToMany(mappedBy = "reconAttributeResults")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ReconAttributeLevelBreaks> reconAttributeLevelBreaks = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public ReconAttributeResults attributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
        return this;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getSummaryJson() {
        return summaryJson;
    }

    public ReconAttributeResults summaryJson(String summaryJson) {
        this.summaryJson = summaryJson;
        return this;
    }

    public void setSummaryJson(String summaryJson) {
        this.summaryJson = summaryJson;
    }

    public ReconResults getReconResults() {
        return reconResults;
    }

    public ReconAttributeResults reconResults(ReconResults reconResults) {
        this.reconResults = reconResults;
        return this;
    }

    public void setReconResults(ReconResults reconResults) {
        this.reconResults = reconResults;
    }

    public Set<ReconAttributeLevelBreaks> getReconAttributeLevelBreaks() {
        return reconAttributeLevelBreaks;
    }

    public ReconAttributeResults reconAttributeLevelBreaks(Set<ReconAttributeLevelBreaks> reconAttributeLevelBreaks) {
        this.reconAttributeLevelBreaks = reconAttributeLevelBreaks;
        return this;
    }

    public ReconAttributeResults addReconAttributeLevelBreaks(ReconAttributeLevelBreaks reconAttributeLevelBreaks) {
        this.reconAttributeLevelBreaks.add(reconAttributeLevelBreaks);
        reconAttributeLevelBreaks.setReconAttributeResults(this);
        return this;
    }

    public ReconAttributeResults removeReconAttributeLevelBreaks(ReconAttributeLevelBreaks reconAttributeLevelBreaks) {
        this.reconAttributeLevelBreaks.remove(reconAttributeLevelBreaks);
        reconAttributeLevelBreaks.setReconAttributeResults(null);
        return this;
    }

    public void setReconAttributeLevelBreaks(Set<ReconAttributeLevelBreaks> reconAttributeLevelBreaks) {
        this.reconAttributeLevelBreaks = reconAttributeLevelBreaks;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconAttributeResults reconAttributeResults = (ReconAttributeResults) o;
        if (reconAttributeResults.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconAttributeResults.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconAttributeResults{" +
            "id=" + getId() +
            ", attributeValue='" + getAttributeValue() + "'" +
            ", summaryJson='" + getSummaryJson() + "'" +
            "}";
    }
}
