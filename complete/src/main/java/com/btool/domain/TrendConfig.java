package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TrendConfig.
 */
@Entity
@Table(name = "td_trend_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TrendConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Size(max = 50)
    @Column(name = "column_name", length = 50)
    private String columnName;

    @Column(name = "rule_json")
    private String ruleJson;

    @Column(name = "tolerance")
    private Float tolerance;

    @ManyToOne
    @JsonIgnoreProperties("trendConfigs")
    private Dataset dataset;

    @OneToMany(mappedBy = "trendConfig")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TrendResults> trendResults = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("trendConfigs")
    private TrendRule trendRule;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public TrendConfig isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getColumnName() {
        return columnName;
    }

    public TrendConfig columnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getRuleJson() {
        return ruleJson;
    }

    public TrendConfig ruleJson(String ruleJson) {
        this.ruleJson = ruleJson;
        return this;
    }

    public void setRuleJson(String ruleJson) {
        this.ruleJson = ruleJson;
    }

    public Float getTolerance() {
        return tolerance;
    }

    public TrendConfig tolerance(Float tolerance) {
        this.tolerance = tolerance;
        return this;
    }

    public void setTolerance(Float tolerance) {
        this.tolerance = tolerance;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public TrendConfig dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public Set<TrendResults> getTrendResults() {
        return trendResults;
    }

    public TrendConfig trendResults(Set<TrendResults> trendResults) {
        this.trendResults = trendResults;
        return this;
    }

    public TrendConfig addTrendResults(TrendResults trendResults) {
        this.trendResults.add(trendResults);
        trendResults.setTrendConfig(this);
        return this;
    }

    public TrendConfig removeTrendResults(TrendResults trendResults) {
        this.trendResults.remove(trendResults);
        trendResults.setTrendConfig(null);
        return this;
    }

    public void setTrendResults(Set<TrendResults> trendResults) {
        this.trendResults = trendResults;
    }

    public TrendRule getTrendRule() {
        return trendRule;
    }

    public TrendConfig trendRule(TrendRule trendRule) {
        this.trendRule = trendRule;
        return this;
    }

    public void setTrendRule(TrendRule trendRule) {
        this.trendRule = trendRule;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrendConfig trendConfig = (TrendConfig) o;
        if (trendConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trendConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TrendConfig{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", columnName='" + getColumnName() + "'" +
            ", ruleJson='" + getRuleJson() + "'" +
            ", tolerance=" + getTolerance() +
            "}";
    }
}
