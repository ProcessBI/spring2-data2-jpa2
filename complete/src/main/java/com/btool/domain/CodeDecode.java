package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CodeDecode.
 */
@Entity
@Table(name = "td_code_decode")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CodeDecode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "short_txt", length = 30, nullable = false)
    private String shortTxt;

    @Size(max = 250)
    @Column(name = "long_txt", length = 250)
    private String longTxt;

    @Column(name = "display_sequence")
    private Integer displaySequence;

    @ManyToOne
    @JsonIgnoreProperties("codeDecodes")
    private RefdataContext refdataContext;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public CodeDecode isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getShortTxt() {
        return shortTxt;
    }

    public CodeDecode shortTxt(String shortTxt) {
        this.shortTxt = shortTxt;
        return this;
    }

    public void setShortTxt(String shortTxt) {
        this.shortTxt = shortTxt;
    }

    public String getLongTxt() {
        return longTxt;
    }

    public CodeDecode longTxt(String longTxt) {
        this.longTxt = longTxt;
        return this;
    }

    public void setLongTxt(String longTxt) {
        this.longTxt = longTxt;
    }

    public Integer getDisplaySequence() {
        return displaySequence;
    }

    public CodeDecode displaySequence(Integer displaySequence) {
        this.displaySequence = displaySequence;
        return this;
    }

    public void setDisplaySequence(Integer displaySequence) {
        this.displaySequence = displaySequence;
    }

    public RefdataContext getRefdataContext() {
        return refdataContext;
    }

    public CodeDecode refdataContext(RefdataContext refdataContext) {
        this.refdataContext = refdataContext;
        return this;
    }

    public void setRefdataContext(RefdataContext refdataContext) {
        this.refdataContext = refdataContext;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeDecode codeDecode = (CodeDecode) o;
        if (codeDecode.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), codeDecode.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CodeDecode{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", shortTxt='" + getShortTxt() + "'" +
            ", longTxt='" + getLongTxt() + "'" +
            ", displaySequence=" + getDisplaySequence() +
            "}";
    }
}
