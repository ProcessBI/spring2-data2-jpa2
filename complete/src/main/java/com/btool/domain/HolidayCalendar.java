package com.btool.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A HolidayCalendar.
 */
@Entity
@Table(name = "td_holiday_calendar")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HolidayCalendar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "holiday_year")
    private Integer holidayYear;

    @Column(name = "holiday_date")
    private LocalDate holidayDate;

    @Size(max = 10)
    @Column(name = "country_code", length = 10)
    private String countryCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHolidayYear() {
        return holidayYear;
    }

    public HolidayCalendar holidayYear(Integer holidayYear) {
        this.holidayYear = holidayYear;
        return this;
    }

    public void setHolidayYear(Integer holidayYear) {
        this.holidayYear = holidayYear;
    }

    public LocalDate getHolidayDate() {
        return holidayDate;
    }

    public HolidayCalendar holidayDate(LocalDate holidayDate) {
        this.holidayDate = holidayDate;
        return this;
    }

    public void setHolidayDate(LocalDate holidayDate) {
        this.holidayDate = holidayDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public HolidayCalendar countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HolidayCalendar holidayCalendar = (HolidayCalendar) o;
        if (holidayCalendar.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), holidayCalendar.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HolidayCalendar{" +
            "id=" + getId() +
            ", holidayYear=" + getHolidayYear() +
            ", holidayDate='" + getHolidayDate() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            "}";
    }
}
