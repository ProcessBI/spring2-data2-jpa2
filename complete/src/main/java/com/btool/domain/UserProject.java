package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A UserProject.
 */
@Entity
@Table(name = "td_user_project")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserProject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "user_access", length = 100)
    private String userAccess;

    @ManyToOne
    @JsonIgnoreProperties("userProjects")
    private Project project;

    @ManyToOne
    @JsonIgnoreProperties("userProjects")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAccess() {
        return userAccess;
    }

    public UserProject userAccess(String userAccess) {
        this.userAccess = userAccess;
        return this;
    }

    public void setUserAccess(String userAccess) {
        this.userAccess = userAccess;
    }

    public Project getProject() {
        return project;
    }

    public UserProject project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getUser() {
        return user;
    }

    public UserProject user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserProject userProject = (UserProject) o;
        if (userProject.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userProject.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserProject{" +
            "id=" + getId() +
            ", userAccess='" + getUserAccess() + "'" +
            "}";
    }
}
