package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ReconHighLevelBreaks.
 */
@Entity
@Table(name = "td_recon_high_level_breaks")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReconHighLevelBreaks implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "result_json")
    private String resultJson;

    @ManyToOne
    @JsonIgnoreProperties("reconHighLevelBreaks")
    private ReconResults reconResults;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResultJson() {
        return resultJson;
    }

    public ReconHighLevelBreaks resultJson(String resultJson) {
        this.resultJson = resultJson;
        return this;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    public ReconResults getReconResults() {
        return reconResults;
    }

    public ReconHighLevelBreaks reconResults(ReconResults reconResults) {
        this.reconResults = reconResults;
        return this;
    }

    public void setReconResults(ReconResults reconResults) {
        this.reconResults = reconResults;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReconHighLevelBreaks reconHighLevelBreaks = (ReconHighLevelBreaks) o;
        if (reconHighLevelBreaks.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reconHighLevelBreaks.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReconHighLevelBreaks{" +
            "id=" + getId() +
            ", resultJson='" + getResultJson() + "'" +
            "}";
    }
}
