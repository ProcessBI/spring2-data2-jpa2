package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DatasetColumn.
 */
@Entity
@Table(name = "td_dataset_column")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DatasetColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "column_name", length = 100, nullable = false)
    private String columnName;

    @Size(max = 100)
    @Column(name = "column_alias", length = 100)
    private String columnAlias;

    @Size(max = 20)
    @Column(name = "column_type", length = 20)
    private String columnType;

    @Column(name = "column_size")
    private Integer columnSize;

    @Column(name = "is_key")
    private Boolean isKey;

    @Column(name = "is_date_1")
    private Boolean isDate1;

    @Column(name = "is_date_2")
    private Boolean isDate2;

    @Size(max = 20)
    @Column(name = "date_format", length = 20)
    private String dateFormat;

    @ManyToOne
    @JsonIgnoreProperties("datasetColumns")
    private Dataset dataset;

    @OneToMany(mappedBy = "targetDatasetColumn")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CriticalDataColumnMapping> criticalDataColumnMappings = new HashSet<>();
    @OneToMany(mappedBy = "sourceDatasetColumn")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CriticalDataColumnMapping> sourceCriticalDataColumnMappings = new HashSet<>();
    @OneToOne(mappedBy = "datasetColumn")
    @JsonIgnore
    private DataDictionaryColumn dataDictionaryColumn;

    @ManyToMany(mappedBy = "datasetCols")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<CriticalDataElement> criticalDataElements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColumnName() {
        return columnName;
    }

    public DatasetColumn columnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnAlias() {
        return columnAlias;
    }

    public DatasetColumn columnAlias(String columnAlias) {
        this.columnAlias = columnAlias;
        return this;
    }

    public void setColumnAlias(String columnAlias) {
        this.columnAlias = columnAlias;
    }

    public String getColumnType() {
        return columnType;
    }

    public DatasetColumn columnType(String columnType) {
        this.columnType = columnType;
        return this;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public Integer getColumnSize() {
        return columnSize;
    }

    public DatasetColumn columnSize(Integer columnSize) {
        this.columnSize = columnSize;
        return this;
    }

    public void setColumnSize(Integer columnSize) {
        this.columnSize = columnSize;
    }

    public Boolean isIsKey() {
        return isKey;
    }

    public DatasetColumn isKey(Boolean isKey) {
        this.isKey = isKey;
        return this;
    }

    public void setIsKey(Boolean isKey) {
        this.isKey = isKey;
    }

    public Boolean isIsDate1() {
        return isDate1;
    }

    public DatasetColumn isDate1(Boolean isDate1) {
        this.isDate1 = isDate1;
        return this;
    }

    public void setIsDate1(Boolean isDate1) {
        this.isDate1 = isDate1;
    }

    public Boolean isIsDate2() {
        return isDate2;
    }

    public DatasetColumn isDate2(Boolean isDate2) {
        this.isDate2 = isDate2;
        return this;
    }

    public void setIsDate2(Boolean isDate2) {
        this.isDate2 = isDate2;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public DatasetColumn dateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
        return this;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public DatasetColumn dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public Set<CriticalDataColumnMapping> getCriticalDataColumnMappings() {
        return criticalDataColumnMappings;
    }

    public DatasetColumn criticalDataColumnMappings(Set<CriticalDataColumnMapping> criticalDataColumnMappings) {
        this.criticalDataColumnMappings = criticalDataColumnMappings;
        return this;
    }

    public DatasetColumn addCriticalDataColumnMapping(CriticalDataColumnMapping criticalDataColumnMapping) {
        this.criticalDataColumnMappings.add(criticalDataColumnMapping);
        criticalDataColumnMapping.setTargetDatasetColumn(this);
        return this;
    }

    public DatasetColumn removeCriticalDataColumnMapping(CriticalDataColumnMapping criticalDataColumnMapping) {
        this.criticalDataColumnMappings.remove(criticalDataColumnMapping);
        criticalDataColumnMapping.setTargetDatasetColumn(null);
        return this;
    }

    public void setCriticalDataColumnMappings(Set<CriticalDataColumnMapping> criticalDataColumnMappings) {
        this.criticalDataColumnMappings = criticalDataColumnMappings;
    }

    public Set<CriticalDataColumnMapping> getSourceCriticalDataColumnMappings() {
        return sourceCriticalDataColumnMappings;
    }

    public DatasetColumn sourceCriticalDataColumnMappings(Set<CriticalDataColumnMapping> criticalDataColumnMappings) {
        this.sourceCriticalDataColumnMappings = criticalDataColumnMappings;
        return this;
    }

    public DatasetColumn addSourceCriticalDataColumnMapping(CriticalDataColumnMapping criticalDataColumnMapping) {
        this.sourceCriticalDataColumnMappings.add(criticalDataColumnMapping);
        criticalDataColumnMapping.setSourceDatasetColumn(this);
        return this;
    }

    public DatasetColumn removeSourceCriticalDataColumnMapping(CriticalDataColumnMapping criticalDataColumnMapping) {
        this.sourceCriticalDataColumnMappings.remove(criticalDataColumnMapping);
        criticalDataColumnMapping.setSourceDatasetColumn(null);
        return this;
    }

    public void setSourceCriticalDataColumnMappings(Set<CriticalDataColumnMapping> criticalDataColumnMappings) {
        this.sourceCriticalDataColumnMappings = criticalDataColumnMappings;
    }

    public DataDictionaryColumn getDataDictionaryColumn() {
        return dataDictionaryColumn;
    }

    public DatasetColumn dataDictionaryColumn(DataDictionaryColumn dataDictionaryColumn) {
        this.dataDictionaryColumn = dataDictionaryColumn;
        return this;
    }

    public void setDataDictionaryColumn(DataDictionaryColumn dataDictionaryColumn) {
        this.dataDictionaryColumn = dataDictionaryColumn;
    }

    public Set<CriticalDataElement> getCriticalDataElements() {
        return criticalDataElements;
    }

    public DatasetColumn criticalDataElements(Set<CriticalDataElement> criticalDataElements) {
        this.criticalDataElements = criticalDataElements;
        return this;
    }

    public DatasetColumn addCriticalDataElement(CriticalDataElement criticalDataElement) {
        this.criticalDataElements.add(criticalDataElement);
        criticalDataElement.getDatasetCols().add(this);
        return this;
    }

    public DatasetColumn removeCriticalDataElement(CriticalDataElement criticalDataElement) {
        this.criticalDataElements.remove(criticalDataElement);
        criticalDataElement.getDatasetCols().remove(this);
        return this;
    }

    public void setCriticalDataElements(Set<CriticalDataElement> criticalDataElements) {
        this.criticalDataElements = criticalDataElements;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatasetColumn datasetColumn = (DatasetColumn) o;
        if (datasetColumn.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasetColumn.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatasetColumn{" +
            "id=" + getId() +
            ", columnName='" + getColumnName() + "'" +
            ", columnAlias='" + getColumnAlias() + "'" +
            ", columnType='" + getColumnType() + "'" +
            ", columnSize=" + getColumnSize() +
            ", isKey='" + isIsKey() + "'" +
            ", isDate1='" + isIsDate1() + "'" +
            ", isDate2='" + isIsDate2() + "'" +
            ", dateFormat='" + getDateFormat() + "'" +
            "}";
    }
}
