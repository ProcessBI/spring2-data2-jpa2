package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DatasourceProperties.
 */
@Entity
@Table(name = "td_datasource_properties")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DatasourceProperties implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "prop_name", length = 50, nullable = false)
    private String propName;

    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "prop_value", length = 250, nullable = false)
    private String propValue;

    @Column(name = "prop_sequence")
    private Integer propSequence;

    @ManyToOne
    @JsonIgnoreProperties("datasourceProperties")
    private Datasource datasource;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPropName() {
        return propName;
    }

    public DatasourceProperties propName(String propName) {
        this.propName = propName;
        return this;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public String getPropValue() {
        return propValue;
    }

    public DatasourceProperties propValue(String propValue) {
        this.propValue = propValue;
        return this;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public Integer getPropSequence() {
        return propSequence;
    }

    public DatasourceProperties propSequence(Integer propSequence) {
        this.propSequence = propSequence;
        return this;
    }

    public void setPropSequence(Integer propSequence) {
        this.propSequence = propSequence;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public DatasourceProperties datasource(Datasource datasource) {
        this.datasource = datasource;
        return this;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatasourceProperties datasourceProperties = (DatasourceProperties) o;
        if (datasourceProperties.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datasourceProperties.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatasourceProperties{" +
            "id=" + getId() +
            ", propName='" + getPropName() + "'" +
            ", propValue='" + getPropValue() + "'" +
            ", propSequence=" + getPropSequence() +
            "}";
    }
}
