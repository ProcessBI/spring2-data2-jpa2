package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.btool.domain.enumeration.TrendDataInputType;

import com.btool.domain.enumeration.TrendDataValueType;

/**
 * A TrendResults.
 */
@Entity
@Table(name = "td_trend_results")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TrendResults implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "period_name")
    private LocalDate periodName;

    @Column(name = "period_value")
    private Long periodValue;

    @Size(max = 100)
    @Column(name = "attribute_name", length = 100)
    private String attributeName;

    @Column(name = "attribute_value")
    private Long attributeValue;

    @Enumerated(EnumType.STRING)
    @Column(name = "input_type")
    private TrendDataInputType inputType;

    @Enumerated(EnumType.STRING)
    @Column(name = "value_type")
    private TrendDataValueType valueType;

    @ManyToOne
    @JsonIgnoreProperties("trendResults")
    private TrendConfig trendConfig;

    @ManyToOne
    @JsonIgnoreProperties("trendResults")
    private RunHistory runHistory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPeriodName() {
        return periodName;
    }

    public TrendResults periodName(LocalDate periodName) {
        this.periodName = periodName;
        return this;
    }

    public void setPeriodName(LocalDate periodName) {
        this.periodName = periodName;
    }

    public Long getPeriodValue() {
        return periodValue;
    }

    public TrendResults periodValue(Long periodValue) {
        this.periodValue = periodValue;
        return this;
    }

    public void setPeriodValue(Long periodValue) {
        this.periodValue = periodValue;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public TrendResults attributeName(String attributeName) {
        this.attributeName = attributeName;
        return this;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public Long getAttributeValue() {
        return attributeValue;
    }

    public TrendResults attributeValue(Long attributeValue) {
        this.attributeValue = attributeValue;
        return this;
    }

    public void setAttributeValue(Long attributeValue) {
        this.attributeValue = attributeValue;
    }

    public TrendDataInputType getInputType() {
        return inputType;
    }

    public TrendResults inputType(TrendDataInputType inputType) {
        this.inputType = inputType;
        return this;
    }

    public void setInputType(TrendDataInputType inputType) {
        this.inputType = inputType;
    }

    public TrendDataValueType getValueType() {
        return valueType;
    }

    public TrendResults valueType(TrendDataValueType valueType) {
        this.valueType = valueType;
        return this;
    }

    public void setValueType(TrendDataValueType valueType) {
        this.valueType = valueType;
    }

    public TrendConfig getTrendConfig() {
        return trendConfig;
    }

    public TrendResults trendConfig(TrendConfig trendConfig) {
        this.trendConfig = trendConfig;
        return this;
    }

    public void setTrendConfig(TrendConfig trendConfig) {
        this.trendConfig = trendConfig;
    }

    public RunHistory getRunHistory() {
        return runHistory;
    }

    public TrendResults runHistory(RunHistory runHistory) {
        this.runHistory = runHistory;
        return this;
    }

    public void setRunHistory(RunHistory runHistory) {
        this.runHistory = runHistory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrendResults trendResults = (TrendResults) o;
        if (trendResults.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trendResults.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TrendResults{" +
            "id=" + getId() +
            ", periodName='" + getPeriodName() + "'" +
            ", periodValue=" + getPeriodValue() +
            ", attributeName='" + getAttributeName() + "'" +
            ", attributeValue=" + getAttributeValue() +
            ", inputType='" + getInputType() + "'" +
            ", valueType='" + getValueType() + "'" +
            "}";
    }
}
