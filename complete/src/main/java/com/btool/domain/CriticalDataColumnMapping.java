package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CriticalDataColumnMapping.
 */
@Entity
@Table(name = "td_critical_data_column_mapping")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CriticalDataColumnMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 10)
    @Column(name = "column_role", length = 10)
    private String columnRole;

    @Size(max = 200)
    @Column(name = "description", length = 200)
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("criticalDataColumnMappings")
    private DatasetColumn targetDatasetColumn;

    @ManyToOne
    @JsonIgnoreProperties("sourceCriticalDataColumnMappings")
    private DatasetColumn sourceDatasetColumn;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColumnRole() {
        return columnRole;
    }

    public CriticalDataColumnMapping columnRole(String columnRole) {
        this.columnRole = columnRole;
        return this;
    }

    public void setColumnRole(String columnRole) {
        this.columnRole = columnRole;
    }

    public String getDescription() {
        return description;
    }

    public CriticalDataColumnMapping description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DatasetColumn getTargetDatasetColumn() {
        return targetDatasetColumn;
    }

    public CriticalDataColumnMapping targetDatasetColumn(DatasetColumn datasetColumn) {
        this.targetDatasetColumn = datasetColumn;
        return this;
    }

    public void setTargetDatasetColumn(DatasetColumn datasetColumn) {
        this.targetDatasetColumn = datasetColumn;
    }

    public DatasetColumn getSourceDatasetColumn() {
        return sourceDatasetColumn;
    }

    public CriticalDataColumnMapping sourceDatasetColumn(DatasetColumn datasetColumn) {
        this.sourceDatasetColumn = datasetColumn;
        return this;
    }

    public void setSourceDatasetColumn(DatasetColumn datasetColumn) {
        this.sourceDatasetColumn = datasetColumn;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CriticalDataColumnMapping criticalDataColumnMapping = (CriticalDataColumnMapping) o;
        if (criticalDataColumnMapping.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), criticalDataColumnMapping.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CriticalDataColumnMapping{" +
            "id=" + getId() +
            ", columnRole='" + getColumnRole() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
