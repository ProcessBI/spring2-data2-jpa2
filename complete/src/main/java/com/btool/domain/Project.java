package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Project.
 */
@Entity
@Table(name = "td_project")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 2, max = 30)
    @Column(name = "name", length = 30, nullable = false)
    private String name;

    @Size(max = 100)
    @Column(name = "description", length = 100)
    private String description;

    @OneToMany(mappedBy = "project")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ProjectDatasetMapping> projectDatasetMappings = new HashSet<>();
    @OneToMany(mappedBy = "project")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Dataset> datasets = new HashSet<>();
    @OneToMany(mappedBy = "project")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UserProject> userProjects = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public Project isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public Project name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Project description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ProjectDatasetMapping> getProjectDatasetMappings() {
        return projectDatasetMappings;
    }

    public Project projectDatasetMappings(Set<ProjectDatasetMapping> projectDatasetMappings) {
        this.projectDatasetMappings = projectDatasetMappings;
        return this;
    }

    public Project addProjectDatasetMapping(ProjectDatasetMapping projectDatasetMapping) {
        this.projectDatasetMappings.add(projectDatasetMapping);
        projectDatasetMapping.setProject(this);
        return this;
    }

    public Project removeProjectDatasetMapping(ProjectDatasetMapping projectDatasetMapping) {
        this.projectDatasetMappings.remove(projectDatasetMapping);
        projectDatasetMapping.setProject(null);
        return this;
    }

    public void setProjectDatasetMappings(Set<ProjectDatasetMapping> projectDatasetMappings) {
        this.projectDatasetMappings = projectDatasetMappings;
    }

    public Set<Dataset> getDatasets() {
        return datasets;
    }

    public Project datasets(Set<Dataset> datasets) {
        this.datasets = datasets;
        return this;
    }

    public Project addDataset(Dataset dataset) {
        this.datasets.add(dataset);
        dataset.setProject(this);
        return this;
    }

    public Project removeDataset(Dataset dataset) {
        this.datasets.remove(dataset);
        dataset.setProject(null);
        return this;
    }

    public void setDatasets(Set<Dataset> datasets) {
        this.datasets = datasets;
    }

    public Set<UserProject> getUserProjects() {
        return userProjects;
    }

    public Project userProjects(Set<UserProject> userProjects) {
        this.userProjects = userProjects;
        return this;
    }

    public Project addUserProject(UserProject userProject) {
        this.userProjects.add(userProject);
        userProject.setProject(this);
        return this;
    }

    public Project removeUserProject(UserProject userProject) {
        this.userProjects.remove(userProject);
        userProject.setProject(null);
        return this;
    }

    public void setUserProjects(Set<UserProject> userProjects) {
        this.userProjects = userProjects;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Project project = (Project) o;
        if (project.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), project.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Project{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
