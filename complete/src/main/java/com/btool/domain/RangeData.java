package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RangeData.
 */
@Entity
@Table(name = "td_range_data")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RangeData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 20)
    @Column(name = "start_value", length = 20)
    private String startValue;

    @Size(max = 20)
    @Column(name = "end_value", length = 20)
    private String endValue;

    @ManyToOne
    @JsonIgnoreProperties("rangeData")
    private RangeDefinition rangeDefinition;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartValue() {
        return startValue;
    }

    public RangeData startValue(String startValue) {
        this.startValue = startValue;
        return this;
    }

    public void setStartValue(String startValue) {
        this.startValue = startValue;
    }

    public String getEndValue() {
        return endValue;
    }

    public RangeData endValue(String endValue) {
        this.endValue = endValue;
        return this;
    }

    public void setEndValue(String endValue) {
        this.endValue = endValue;
    }

    public RangeDefinition getRangeDefinition() {
        return rangeDefinition;
    }

    public RangeData rangeDefinition(RangeDefinition rangeDefinition) {
        this.rangeDefinition = rangeDefinition;
        return this;
    }

    public void setRangeDefinition(RangeDefinition rangeDefinition) {
        this.rangeDefinition = rangeDefinition;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RangeData rangeData = (RangeData) o;
        if (rangeData.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rangeData.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RangeData{" +
            "id=" + getId() +
            ", startValue='" + getStartValue() + "'" +
            ", endValue='" + getEndValue() + "'" +
            "}";
    }
}
