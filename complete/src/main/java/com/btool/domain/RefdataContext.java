package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A RefdataContext.
 */
@Entity
@Table(name = "td_refdata_context")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RefdataContext implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "short_txt", length = 30, nullable = false)
    private String shortTxt;

    @Size(max = 250)
    @Column(name = "long_txt", length = 250)
    private String longTxt;

    @OneToMany(mappedBy = "refdataContext")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CodeDecode> codeDecodes = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public RefdataContext isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getShortTxt() {
        return shortTxt;
    }

    public RefdataContext shortTxt(String shortTxt) {
        this.shortTxt = shortTxt;
        return this;
    }

    public void setShortTxt(String shortTxt) {
        this.shortTxt = shortTxt;
    }

    public String getLongTxt() {
        return longTxt;
    }

    public RefdataContext longTxt(String longTxt) {
        this.longTxt = longTxt;
        return this;
    }

    public void setLongTxt(String longTxt) {
        this.longTxt = longTxt;
    }

    public Set<CodeDecode> getCodeDecodes() {
        return codeDecodes;
    }

    public RefdataContext codeDecodes(Set<CodeDecode> codeDecodes) {
        this.codeDecodes = codeDecodes;
        return this;
    }

    public RefdataContext addCodeDecode(CodeDecode codeDecode) {
        this.codeDecodes.add(codeDecode);
        codeDecode.setRefdataContext(this);
        return this;
    }

    public RefdataContext removeCodeDecode(CodeDecode codeDecode) {
        this.codeDecodes.remove(codeDecode);
        codeDecode.setRefdataContext(null);
        return this;
    }

    public void setCodeDecodes(Set<CodeDecode> codeDecodes) {
        this.codeDecodes = codeDecodes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RefdataContext refdataContext = (RefdataContext) o;
        if (refdataContext.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), refdataContext.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RefdataContext{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", shortTxt='" + getShortTxt() + "'" +
            ", longTxt='" + getLongTxt() + "'" +
            "}";
    }
}
