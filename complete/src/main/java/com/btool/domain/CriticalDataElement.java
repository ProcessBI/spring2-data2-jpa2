package com.btool.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CriticalDataElement.
 */
@Entity
@Table(name = "td_critical_data_element")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CriticalDataElement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "data_family", length = 100)
    private String dataFamily;

    @Size(max = 100)
    @Column(name = "data_domain", length = 100)
    private String dataDomain;

    @Size(max = 100)
    @Column(name = "data_sub_domain", length = 100)
    private String dataSubDomain;

    @Size(max = 100)
    @Column(name = "data_field", length = 100)
    private String dataField;

    @Size(max = 500)
    @Column(name = "descritption", length = 500)
    private String descritption;

    @Column(name = "is_cde")
    private Boolean isCde;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "td_critical_data_element_dataset_col",
               joinColumns = @JoinColumn(name = "critical_data_elements_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "dataset_cols_id", referencedColumnName = "id"))
    private Set<DatasetColumn> datasetCols = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataFamily() {
        return dataFamily;
    }

    public CriticalDataElement dataFamily(String dataFamily) {
        this.dataFamily = dataFamily;
        return this;
    }

    public void setDataFamily(String dataFamily) {
        this.dataFamily = dataFamily;
    }

    public String getDataDomain() {
        return dataDomain;
    }

    public CriticalDataElement dataDomain(String dataDomain) {
        this.dataDomain = dataDomain;
        return this;
    }

    public void setDataDomain(String dataDomain) {
        this.dataDomain = dataDomain;
    }

    public String getDataSubDomain() {
        return dataSubDomain;
    }

    public CriticalDataElement dataSubDomain(String dataSubDomain) {
        this.dataSubDomain = dataSubDomain;
        return this;
    }

    public void setDataSubDomain(String dataSubDomain) {
        this.dataSubDomain = dataSubDomain;
    }

    public String getDataField() {
        return dataField;
    }

    public CriticalDataElement dataField(String dataField) {
        this.dataField = dataField;
        return this;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDescritption() {
        return descritption;
    }

    public CriticalDataElement descritption(String descritption) {
        this.descritption = descritption;
        return this;
    }

    public void setDescritption(String descritption) {
        this.descritption = descritption;
    }

    public Boolean isIsCde() {
        return isCde;
    }

    public CriticalDataElement isCde(Boolean isCde) {
        this.isCde = isCde;
        return this;
    }

    public void setIsCde(Boolean isCde) {
        this.isCde = isCde;
    }

    public Set<DatasetColumn> getDatasetCols() {
        return datasetCols;
    }

    public CriticalDataElement datasetCols(Set<DatasetColumn> datasetColumns) {
        this.datasetCols = datasetColumns;
        return this;
    }

    public CriticalDataElement addDatasetCol(DatasetColumn datasetColumn) {
        this.datasetCols.add(datasetColumn);
        datasetColumn.getCriticalDataElements().add(this);
        return this;
    }

    public CriticalDataElement removeDatasetCol(DatasetColumn datasetColumn) {
        this.datasetCols.remove(datasetColumn);
        datasetColumn.getCriticalDataElements().remove(this);
        return this;
    }

    public void setDatasetCols(Set<DatasetColumn> datasetColumns) {
        this.datasetCols = datasetColumns;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CriticalDataElement criticalDataElement = (CriticalDataElement) o;
        if (criticalDataElement.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), criticalDataElement.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CriticalDataElement{" +
            "id=" + getId() +
            ", dataFamily='" + getDataFamily() + "'" +
            ", dataDomain='" + getDataDomain() + "'" +
            ", dataSubDomain='" + getDataSubDomain() + "'" +
            ", dataField='" + getDataField() + "'" +
            ", descritption='" + getDescritption() + "'" +
            ", isCde='" + isIsCde() + "'" +
            "}";
    }
}
