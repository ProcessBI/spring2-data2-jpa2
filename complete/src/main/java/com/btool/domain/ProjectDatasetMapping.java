package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProjectDatasetMapping.
 */
@Entity
@Table(name = "td_project_dataset_mapping")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjectDatasetMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Column(name = "x_axis", nullable = false)
    private Integer xAxis;

    @NotNull
    @Column(name = "y_axis", nullable = false)
    private Integer yAxis;

    @ManyToOne
    @JsonIgnoreProperties("projectDatasetMappings")
    private Dataset dataset;

    @ManyToOne
    @JsonIgnoreProperties("projectDatasetMappings")
    private Project project;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public ProjectDatasetMapping isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getxAxis() {
        return xAxis;
    }

    public ProjectDatasetMapping xAxis(Integer xAxis) {
        this.xAxis = xAxis;
        return this;
    }

    public void setxAxis(Integer xAxis) {
        this.xAxis = xAxis;
    }

    public Integer getyAxis() {
        return yAxis;
    }

    public ProjectDatasetMapping yAxis(Integer yAxis) {
        this.yAxis = yAxis;
        return this;
    }

    public void setyAxis(Integer yAxis) {
        this.yAxis = yAxis;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public ProjectDatasetMapping dataset(Dataset dataset) {
        this.dataset = dataset;
        return this;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public Project getProject() {
        return project;
    }

    public ProjectDatasetMapping project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectDatasetMapping projectDatasetMapping = (ProjectDatasetMapping) o;
        if (projectDatasetMapping.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), projectDatasetMapping.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProjectDatasetMapping{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", xAxis=" + getxAxis() +
            ", yAxis=" + getyAxis() +
            "}";
    }
}
