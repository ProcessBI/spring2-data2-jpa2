package com.btool.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A AuditLog.
 */
@Entity
@Table(name = "td_audit_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "table_name", length = 100)
    private String tableName;

    @Column(name = "key_value")
    private Long keyValue;

    @Column(name = "change_time_stamp")
    private ZonedDateTime changeTimeStamp;

    @Size(max = 15)
    @Column(name = "ip_address", length = 15)
    private String ipAddress;

    @Column(name = "old_value_json")
    private String oldValueJson;

    @Column(name = "new_value_json")
    private String newValueJson;

    @ManyToOne
    @JsonIgnoreProperties("auditLogs")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public AuditLog tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Long getKeyValue() {
        return keyValue;
    }

    public AuditLog keyValue(Long keyValue) {
        this.keyValue = keyValue;
        return this;
    }

    public void setKeyValue(Long keyValue) {
        this.keyValue = keyValue;
    }

    public ZonedDateTime getChangeTimeStamp() {
        return changeTimeStamp;
    }

    public AuditLog changeTimeStamp(ZonedDateTime changeTimeStamp) {
        this.changeTimeStamp = changeTimeStamp;
        return this;
    }

    public void setChangeTimeStamp(ZonedDateTime changeTimeStamp) {
        this.changeTimeStamp = changeTimeStamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public AuditLog ipAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getOldValueJson() {
        return oldValueJson;
    }

    public AuditLog oldValueJson(String oldValueJson) {
        this.oldValueJson = oldValueJson;
        return this;
    }

    public void setOldValueJson(String oldValueJson) {
        this.oldValueJson = oldValueJson;
    }

    public String getNewValueJson() {
        return newValueJson;
    }

    public AuditLog newValueJson(String newValueJson) {
        this.newValueJson = newValueJson;
        return this;
    }

    public void setNewValueJson(String newValueJson) {
        this.newValueJson = newValueJson;
    }

    public User getUser() {
        return user;
    }

    public AuditLog user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuditLog auditLog = (AuditLog) o;
        if (auditLog.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), auditLog.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuditLog{" +
            "id=" + getId() +
            ", tableName='" + getTableName() + "'" +
            ", keyValue=" + getKeyValue() +
            ", changeTimeStamp='" + getChangeTimeStamp() + "'" +
            ", ipAddress='" + getIpAddress() + "'" +
            ", oldValueJson='" + getOldValueJson() + "'" +
            ", newValueJson='" + getNewValueJson() + "'" +
            "}";
    }
}
